import {Card, CardContent, InputAdornment} from "@material-ui/core";
import {FullWidthTextField} from "./FullWidthTextField";
import React, {useEffect, useState} from "react";
import {FaSearch} from "react-icons/all";

export const SearchSection = (props: { label: string, defaultTerm?: string, onChange: (search: string) => void }) => {
  const defaultTerm = props.defaultTerm || ""
  const [search, setSearch] = useState(defaultTerm)
  const [lastTerm, setLastTerm] = useState(defaultTerm)

  useEffect(() => {
    if (search !== lastTerm) {
      let timeout: NodeJS.Timeout = setTimeout(() => {
        setLastTerm(search)
        props.onChange(search)
      }, 300)
      return () => {
        clearTimeout(timeout)
      }
    }
  }, [search, props, lastTerm])


  return <FullWidthTextField
    InputProps={{
      startAdornment: (
        <InputAdornment position="start">
          <FaSearch/>
        </InputAdornment>
      ),
    }}
    label={props.label}
    value={search}
    onChange={(ev) => setSearch(ev.target.value)}
  />
}

export const SearchCard = (props: { label: string, defaultTerm?: string, onChange: (search: string) => void }) => {
  return <Card>
    <CardContent>
      <SearchSection {...props}/>
    </CardContent>
  </Card>
}
