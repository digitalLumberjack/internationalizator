import {Box, LinearProgress, LinearProgressProps, Typography} from "@material-ui/core";


export const LinearProgressWithLabel = (props: LinearProgressProps & { value: number, extraText?: string }) => {
  return (
    <Box display="flex" alignItems="center">
      <Box mr={1}>
        <Typography>{`${Math.round(
          props.value,
        )}% ${props.extraText ?? ""}`}</Typography>
      </Box>
      <Box flexGrow={1}>
        <LinearProgress variant="determinate" {...props} />
      </Box>

    </Box>
  );
}
