import {Container, styled} from "@material-ui/core";

export const CenteredContainer = styled(Container)({
  width: "100%",
  textAlign: "center",
  paddingTop: "40vh"
})
