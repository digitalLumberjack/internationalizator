import {Grid, styled} from "@material-ui/core";

export const CenteredGrid = styled(Grid)({
  textAlign: "center"
})
