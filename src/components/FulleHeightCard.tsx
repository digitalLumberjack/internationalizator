import {Card, styled} from "@material-ui/core";

export const FullHeightCard = styled(Card)({
  height: "100%"
})
