import {Chip, styled} from "@material-ui/core";
import {green, red} from "@material-ui/core/colors";

export const OkChip = styled(Chip)((props) => ({
  backgroundColor: green["A200"],
  color: props.theme.palette.getContrastText(green.A700)
}))

export const NokChip = styled(Chip)((props) => ({
  backgroundColor: red.A100,
  color: props.theme.palette.getContrastText(red.A100)
}))
