import React, {useEffect, useState} from "react";
import {gitlabApi} from "../api/gitlabApi";
import {Avatar} from "@material-ui/core";

export type AvatarAndNameAware = { avatar_url: string, name: string }

export const ElementAvatar = (props: { token?: string, url: string }) => {
  const {url, token} = props
  const [info, setInfo] = useState<AvatarAndNameAware | null>(null)
  useEffect(() => {
    if (token) {
      gitlabApi(token).createFetch(url).then(data => {
        setInfo(data)
      })
    }
  }, [token, url])

  return info?.avatar_url ? <Avatar src={info.avatar_url}/> : <Avatar>{info?.name.substr(0, 1)}</Avatar>
}

export const GroupAvatar = (props: { token?: string, groupId: string }) => {
  const url = `/groups/${props.groupId}`
  return <ElementAvatar url={url} token={props.token}/>
}

export const UserAvatar = (props: { token?: string }) => {
  const url = `/user`
  return <ElementAvatar url={url} token={props.token}/>
}
export const ProjectAvatar = (props: { token?: string, projectId: string }) => {
  const url = `/projects/${props.projectId}`
  return <ElementAvatar url={url} token={props.token}/>
}

