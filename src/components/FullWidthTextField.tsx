import {styled, TextField} from "@material-ui/core";

export const FullWidthTextField = styled(TextField)({
  width: "100%",
  marginBottom: "1em",
})
