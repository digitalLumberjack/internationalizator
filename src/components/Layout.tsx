import React from "react";
import {Container} from "@material-ui/core";

export const Layout = (props: { children: React.ReactNode, noContainer?: boolean }) => {
  if (props.noContainer) {
    return <React.Fragment>{props.children}</React.Fragment>
  }
  return (
    <Container>
      <React.Fragment>
        {props.children}
      </React.Fragment>
    </Container>
  )
}
