import {Grid, styled} from "@material-ui/core";

export const GrowGrid = styled(Grid)({
  flexGrow: 1
})
