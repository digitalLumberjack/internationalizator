export type GitlabProjectInfo = { id: string, name: string, description: string, web_url: string, permissions: { group_access: { access_level: number }, project_access: { access_level: number } } }
export type GitlabGroupInfo = { id: string, avatar_url?: string, name: string, full_path: string, description: string, web_url: string, permissions: { group_access: { access_level: number }, project_access: { access_level: number } } }
export type GitlabBranchInfo = { name: string, can_push: boolean, web_url: string }
export type GitlabTreeInfo = { id: string, name: string, path: string, type: string }
