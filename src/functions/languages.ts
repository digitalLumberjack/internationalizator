import {Handler} from "@netlify/functions";
import FormData from "form-data";
import fetch from "node-fetch";

export const deeplProxy = async (endpoint: string, params?: { [key: string]: string }): Promise<{ body: string, statusCode: number }> => {
  const key = process.env.DEEPL_API_KEY
  const deeplApiEndpoint = process.env.DEEPL_API_ENDPOINT

  if (!key || !deeplApiEndpoint) {
    return {
      statusCode: 500,
      body: "Deepl environment variable not set",
    };
  }

  const formData = new FormData();
  formData.append("auth_key", key)
  Object.entries(params || {}).map(param => formData.append(param[0], param[1]))

  return fetch(`${deeplApiEndpoint}${endpoint}`, {
    method: "POST",
    body: formData
  }).then(res => {
    if (res.status >= 400) {
      return Promise.reject("Unable to fetch from deepl")
    } else {
      return res.json()
    }
  }, e => {
    return Promise.reject("Unable to fetch from deepl")
  }).then(data => {
    return {
      statusCode: 200,
      body: JSON.stringify(data)
    }
  })
};

const handler: Handler = async () => {
  return deeplProxy("languages", {type: "target"})
};

export {handler}
