import {Handler} from "@netlify/functions";
import {Event, EventHeaders} from "@netlify/functions/src/function/event";
import {Context} from "@netlify/functions/src/function/context";
import {HandlerCallback} from "@netlify/functions/src/function/handler";
import {deeplProxy} from "./languages";
import {Base64} from "js-base64";

function getValueIgnoringKeyCase(object: EventHeaders, key: string) {
  const foundKey = Object
    .keys(object)
    .find(currentKey => currentKey.toLocaleLowerCase() === key.toLowerCase());
  return foundKey ? object[foundKey] : undefined;
}

function getBoundary(event: Event) {
  return getValueIgnoringKeyCase(event.headers, 'Content-Type')?.split('=')[1];
}

const parse = (event: Event) => {
  const boundary = getBoundary(event);
  const result: { [key: string]: string } = {};
  if (event.body && boundary) {
    const body = event.isBase64Encoded ? Base64.decode(event.body) : event.body
    body.split(boundary)
      .forEach(item => {
        if (/name=".+"/g.test(item)) {
          const match = item.match(/name=".+"/g)
          if (match) {
            result[match[0].slice(6, -1)] = item.slice(item.search(/name=".+"/g) + match[0].length + 4, -4);
          }
        }
      });
  }
  return result;
}

const handler: Handler = async (event: Event, context: Context, callback: HandlerCallback) => {
  if (event.body != null) {
    const parsed = parse(event)
    console.log(parsed)
    return deeplProxy("translate", parsed)
  } else return {
    statusCode: 400
  }

}

export {handler}
