export const getLocalStorage = <T>(name: string) => {
  return {
    get: (): T | undefined => {
      const item = localStorage.getItem(name)
      return item ? JSON.parse(item) : undefined
    },
    set: (element: T) => {
      localStorage.setItem(name, JSON.stringify(element))
    },
    remove: () => {
      localStorage.removeItem(name)
    }
  }
}
