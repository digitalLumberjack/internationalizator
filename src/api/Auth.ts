import cryptoRandomString from "crypto-random-string";
import {Base64} from "js-base64";
import {sha256} from "js-sha256";
import {gitlabApi} from "./gitlabApi";
import {getLocalStorage} from "./getLocalStorage";

export type OauthSingleConnectInfo = {
  APP_ID: string,
  APP_SECRET: string,
  REDIRECT_URI: string,
  CODE_VERIFIER: string,
  CODE_CHALLENGE: string,
  STATE: string,
  SCOPES: string,
}

export type Provider = "gitlab" | "github"
export const createOauthSingleConnectInfo = (provider: Provider): OauthSingleConnectInfo | null => {
  const SCOPES = provider === "gitlab" ? "api" : "repos"
  const CODE_VERIFIER = cryptoRandomString({length: 50})
  const APP_ID = process.env[`REACT_APP_APP_ID_${provider.toUpperCase()}`]
  const APP_SECRET = process.env[`REACT_APP_APP_SECRET_${provider.toUpperCase()}`]
  const REDIRECT_URI = `${process.env.REACT_APP_REDIRECT_URI}/${provider}`
  if (APP_ID && APP_SECRET && REDIRECT_URI) {
    return {
      APP_ID,
      APP_SECRET,
      REDIRECT_URI,
      SCOPES,
      CODE_VERIFIER,
      CODE_CHALLENGE: Base64.encode(sha256(CODE_VERIFIER), true),
      STATE: cryptoRandomString({length: 5})
    }
  }
  return null
}
export type UserInfo = { token: string, userId: string, userName: string, userEmail: string, provider: Provider }

export type AuthProvider = { logout: () => void, getUserInfo: () => UserInfo | undefined, getOauthUrl: () => string, verify: (code: string, state: string) => Promise<UserInfo | undefined> }

export const getAuthProvider = (provider: "gitlab" | "github", reset?: boolean): AuthProvider | null => {
  const infoStorage = getLocalStorage<OauthSingleConnectInfo>(`${provider}i17rProviderOauthInfo`)
  const oauthInfo = reset ? createOauthSingleConnectInfo(provider) : (infoStorage.get() || createOauthSingleConnectInfo(provider))
  if (!oauthInfo)
    return null
  infoStorage.set(oauthInfo)

  const dataStorage = getLocalStorage<UserInfo>(`${provider}i17rUserInfo`)
  let loggedData: UserInfo | undefined = reset ? undefined : (dataStorage.get())
  if (loggedData) {
    dataStorage.set(loggedData)
  } else {
    dataStorage.remove()
  }

  if (provider === "gitlab") {
    return {
      logout: () => {
        infoStorage.remove()
        dataStorage.remove()
      },
      getUserInfo: () => {
        return loggedData
      },
      getOauthUrl: () => {
        return `https://gitlab.com/oauth/authorize?client_id=${oauthInfo.APP_ID}&redirect_uri=${oauthInfo.REDIRECT_URI}&response_type=code&state=${oauthInfo.STATE}&scope=${oauthInfo.SCOPES}`
      },
      verify: (code: string, state: string): Promise<UserInfo> => {
        if (state === oauthInfo.STATE) {
          const body = {
            client_id: oauthInfo.APP_ID,
            client_secret: oauthInfo.APP_SECRET,
            code: code,
            grant_type: "authorization_code",
            redirect_uri: oauthInfo.REDIRECT_URI,
            //code_verifier: oauthInfo.CODE_VERIFIER
          }
          const requestOptions = {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(body)
          };
          const url = `https://gitlab.com/oauth/token`
          return fetch(url, requestOptions).then(res => res.json()).then((tokenData) => {
            if (tokenData.access_token) {
              return gitlabApi(tokenData.access_token).createFetch("/user").then(userData => {
                loggedData = {
                  token: tokenData.access_token,
                  userId: userData.id,
                  userName: userData.username,
                  userEmail: userData.email,
                  provider: "gitlab"
                }
                dataStorage.set(loggedData)
                return Promise.resolve(loggedData)
              }, () => {
                return Promise.reject("Unable to login user with gitlab !")
              })
            } else {
              return Promise.reject("Unable to login user with gitlab !")
            }
          }, () => {
            return Promise.reject("Unable to login user with gitlab !")
          })
        } else {
          return Promise.reject("Unable to login user with gitlab !")
        }
      }
    }
  } else {
    return {
      logout: () => {
        infoStorage.remove()
        dataStorage.remove()
      },
      getUserInfo: () => {
        return loggedData
      },
      getOauthUrl: () => {
        return ` https://github.com/login/oauth/authorize?client_id=${oauthInfo.APP_ID}&redirect_uri=${oauthInfo.REDIRECT_URI}&state=${oauthInfo.STATE}&scope==${oauthInfo.SCOPES}`
      },
      verify: (code: string, state: string): Promise<UserInfo> => {
        if (state === oauthInfo.STATE) {
          const body = {
            client_id: oauthInfo.APP_ID,
            client_secret: oauthInfo.APP_SECRET,
            code: code,
            redirect_uri: oauthInfo.REDIRECT_URI,
          }
          const requestOptions = {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(body)
          };
          const url = `https://github.com/login/oauth/access_token`
          return fetch(url, requestOptions).then(res => res.json()).then((tokenData) => {
            console.log(tokenData)
            if (tokenData.access_token) {
              // return gitlabApi(tokenData.access_token).createFetch("/user").then(userData => {
              //   loggedData = {
              //     token: tokenData.access_token,
              //     userId: userData.id,
              //     userName: userData.username,
              //     userEmail: userData.email
              //   }
              //   dataStorage.set(loggedData)
              //   return Promise.resolve(loggedData)
              // }, () => {
              //   return Promise.reject("Unable to login user with gitlab !")
              // })
              return Promise.reject("Unable to login user with gitlab !")
            } else {
              return Promise.reject("Unable to login user with gitlab !")
            }
          }, () => {
            return Promise.reject("Unable to login user with gitlab !")
          })
        } else {
          return Promise.reject("Unable to login user with gitlab !")
        }
      },
    }
  }
}


export const logout = () => {
  getAuthProvider("gitlab")?.logout()
  getAuthProvider("github")?.logout()
}
