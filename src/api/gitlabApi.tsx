export const gitlabApi = (token?: string) => {
  return {
    createFetch: (endpoint: string) => {
      const headers = token ? {headers: {"Authorization": `Bearer ${token}`}} : {}
      return fetch(`https://gitlab.com/api/v4${endpoint}`, headers).then(res => {
        if (res.status < 400) {
          return res.json()
        } else {
          throw new Error("HTTP Status : " + res.status)
        }
      })
    },

    createUpdate: (endpoint: string, content: string, gitRef: string, email: string, name: string, message: string) => {
      const body = {
        branch: gitRef,
        author_email: email,
        author_name: name,
        commit_message: message,
        content,
      }
      return fetch(`https://gitlab.com/api/v4${endpoint}`, {
        method: "PUT",
        headers: {
          "Authorization": `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(body)
      }).then(res => {
        if (res.status < 400) {
          return res.json()
        } else {
          throw new Error("HTTP Status : " + res.status)
        }
      })
    }
  }
}
