import {ContentProcessor, Translations} from "./ContentProcessor";

export const jsonProcessor: ContentProcessor = {
  contentToTranslations: (content: string): Translations => {
    const translations: Translations = {meta: {}, translations: {}}
    const decodedFileJson = JSON.parse(content)
    for (const entry of Object.entries(decodedFileJson)) {
      const tid = entry[0]
      const tvalue = entry[1] as string
      translations.translations[tid] = {
        id: tid,
        str: tvalue,
      }
    }
    return translations
  },
  translationsToContent: (translations: Translations) => {
    let json: { [key: string]: string } = {}
    for (const trans of Object.entries(translations.translations)) {
      json[trans[0]] = trans[1].str
    }
    return JSON.stringify(json, null, 2)
  }
}
