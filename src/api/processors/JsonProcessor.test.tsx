import {Translations} from "./ContentProcessor";
import {jsonProcessor} from "./JsonProcessor";

describe("The Json Processor translationsToContent", () => {

  it("should return a json string from a single translation", () => {
    const trans: Translations = {
      meta: {},
      translations: {
        "first": {
          id: "first",
          str: "firststr"
        }
      }
    }
    expect(jsonProcessor.translationsToContent(trans)).toEqual('{\n  "first": "firststr"\n}')
  })

  it("should return a json from two translations", () => {
    const trans: Translations = {
      meta: {},
      translations: {
        "first": {
          id: "first",
          str: "firststr"
        },
        "second": {
          id: "second",
          str: "secondstr"
        }
      }
    }
    expect(jsonProcessor.translationsToContent(trans)).toEqual('{\n  "first": "firststr",\n  "second": "secondstr"\n}')
  })

})

describe("The Json Processor", () => {
  it("should keep a file the same if no modification is made to translations", () => {
    const fileContent = '{\n  "first": "firststr",\n  "second": "secondstr"\n}'
    expect(jsonProcessor.translationsToContent(jsonProcessor.contentToTranslations(fileContent))).toEqual(fileContent)
  })
})
