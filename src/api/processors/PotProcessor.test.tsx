import {potProcessor} from "./PotProcessor";
import {Translations} from "./ContentProcessor";

describe("The Pot Processor contentToTranslations", () => {

  it("should process a simple translation", () => {
    const trans = potProcessor.contentToTranslations(`msgid "AN UPDATE IS AVAILABLE FOR YOUR RECALBOX"
msgstr "UNE MISE À JOUR EST DISPONIBLE POUR VOTRE RECALBOX"
`)
    expect(trans.translations).toEqual({
      "AN UPDATE IS AVAILABLE FOR YOUR RECALBOX": {
        "id": "AN UPDATE IS AVAILABLE FOR YOUR RECALBOX",
        "idPlural": undefined,
        "str": "UNE MISE À JOUR EST DISPONIBLE POUR VOTRE RECALBOX",
        "str_plural": undefined
      }
    })
  })

  it("should process two translations", () => {
    const trans = potProcessor.contentToTranslations(`msgid "SEARCH"
msgstr "RECHERCHER"

msgid "SCRAPING IN PROGRESS"
msgstr "SCRAPING EN COURS"
`)
    expect(Object.entries(trans.translations).length).toEqual(2)
  })

  it("should process metadatas", () => {
    const trans = potProcessor.contentToTranslations(`msgid ""
msgstr ""
"Project-Id-Version: recalbox-emulationstation\\n"
"Language: fr\\n"
"MIME-Version: 1.0\\n"
"Content-Type: text/plain; charset=UTF-8\\n"
"Content-Transfer-Encoding: 8bit\\n"
"X-Generator: POEditor.com\\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\\n"

msgid "AN UPDATE IS AVAILABLE FOR YOUR RECALBOX"
msgstr "UNE MISE À JOUR EST DISPONIBLE POUR VOTRE RECALBOX"

`)
    expect(Object.entries(trans.translations).length).toEqual(1)
    expect(trans.meta).toEqual({
      "Content-Transfer-Encoding": "8bit",
      "Content-Type": "text/plain; charset=UTF-8",
      "Language": "fr",
      "MIME-Version": "1.0",
      "Plural-Forms": "nplurals=2; plural=(n > 1);",
      "Project-Id-Version": "recalbox-emulationstation",
      "X-Generator": "POEditor.com"
    })
  })

  it("should process multilines entries", () => {
    const trans = potProcessor.contentToTranslations(`msgid ""
"WE CAN'T FIND ANY SYSTEMS!\\n"
"CHECK THAT YOUR PATHS ARE CORRECT IN THE SYSTEMS CONFIGURATION FILE, AND "
"YOUR GAME DIRECTORY HAS AT LEAST ONE GAME WITH THE CORRECT EXTENSION.\\n"
"\\n"
"VISIT RECALBOX.COM FOR MORE INFORMATION."
msgstr ""
"AUCUN JEU N'A ÉTÉ TROUVÉ SUR LE SYSTÈME !\\n"
"VEUILLEZ AJOUTER DES JEUX EN SUIVANT LES INSTRUCTIONS DE LA NOTICE.\\n"
"\\n"
"RENDEZ-VOUS SUR RECALBOX.COM POUR PLUS D'INFORMATIONS"
`)
    expect(Object.entries(trans.translations)[0][1]).toEqual({
      "id": `WE CAN'T FIND ANY SYSTEMS!
CHECK THAT YOUR PATHS ARE CORRECT IN THE SYSTEMS CONFIGURATION FILE, AND YOUR GAME DIRECTORY HAS AT LEAST ONE GAME WITH THE CORRECT EXTENSION.

VISIT RECALBOX.COM FOR MORE INFORMATION.`,
      "idPlural": undefined,
      "str": `AUCUN JEU N'A ÉTÉ TROUVÉ SUR LE SYSTÈME !
VEUILLEZ AJOUTER DES JEUX EN SUIVANT LES INSTRUCTIONS DE LA NOTICE.

RENDEZ-VOUS SUR RECALBOX.COM POUR PLUS D'INFORMATIONS`,
      "str_plural": undefined,
    })
  })

  it("should process translation with plurial", () => {
    const trans = potProcessor.contentToTranslations(`msgid "%i GAME SUCCESSFULLY SCRAPED!"
msgid_plural "%i GAMES SUCCESSFULLY SCRAPED!"
msgstr[0] "%i JEU SCRAPÉ !"
msgstr[1] "%i JEUX SCRAPÉS !"
`)
    expect(trans.translations).toEqual({
      "%i GAME SUCCESSFULLY SCRAPED!": {
        "id": "%i GAME SUCCESSFULLY SCRAPED!",
        "idPlural": "%i GAMES SUCCESSFULLY SCRAPED!",
        "str": "%i JEU SCRAPÉ !",
        "str_plural": "%i JEUX SCRAPÉS !"
      }
    })
  })
})


describe("The Pot Processor translationsToContent", () => {

  it("should return a po string from a single translation", () => {
    const trans: Translations = {
      meta: {},
      translations: {
        "first": {
          id: "first",
          str: "firststr"
        }
      }
    }
    expect(potProcessor.translationsToContent(trans)).toEqual(`msgid ""
msgstr "Content-Type: text/plain\\n"

msgid "first"
msgstr "firststr"`)
  })

  it("should return a po string from multiple translation", () => {
    const trans: Translations = {
      meta: {},
      translations: {
        "first": {
          id: "first",
          str: "firststr"
        },
        "second": {
          id: "second",
          str: "secondstr"
        }
      }
    }
    expect(potProcessor.translationsToContent(trans)).toEqual(`msgid ""
msgstr "Content-Type: text/plain\\n"

msgid "first"
msgstr "firststr"

msgid "second"
msgstr "secondstr"`)
  })

  it("should return a po string from a multiline translation", () => {
    const trans: Translations = {
      meta: {},
      translations: {
        "first\nsecondline": {
          id: "first\nsecondline",
          str: "firststr"
        }
      }
    }
    expect(potProcessor.translationsToContent(trans)).toEqual(`msgid ""
msgstr "Content-Type: text/plain\\n"

msgid ""
"first\\n"
"secondline"
msgstr "firststr"`)
  })

  it("should return a po string with metedatas", () => {
    const trans: Translations = {
      meta: {
        "Project-Id-Version": "recalbox-emulationstation",
        "Language": "fr",
        "MIME-Version": "1.0",
        "Content-Type": "text/plain; charset=UTF-8",
        "Content-Transfer-Encoding": "8bit",
        "X-Generator": "internationalizator.io",
        "Plural-Forms": "nplurals=2; plural=(n > 1);"
      },
      translations: {
        "first\nsecondline": {
          id: "first\nsecondline",
          str: "firststr"
        }
      }
    }
    expect(potProcessor.translationsToContent(trans)).toEqual(`msgid ""
msgstr ""
"Project-Id-Version: recalbox-emulationstation\\n"
"Language: fr\\n"
"MIME-Version: 1.0\\n"
"Content-Type: text/plain; charset=utf-8\\n"
"Content-Transfer-Encoding: 8bit\\n"
"X-Generator: internationalizator.io\\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\\n"

msgid ""
"first\\n"
"secondline"
msgstr "firststr"`)
  })

  it("should return a po string from an entry with plurial translations", () => {
    const trans: Translations = {
      meta: {},
      translations: {
        "%i GAME SUCCESSFULLY SCRAPED!": {
          id: "%i GAME SUCCESSFULLY SCRAPED!",
          idPlural: "%i GAMES SUCCESSFULLY SCRAPED!",
          str: "%i JEU SCRAPÉ !",
          strPlural: "%i JEUX SCRAPÉS !"
        }
      }
    }
    expect(potProcessor.translationsToContent(trans)).toEqual(`msgid ""
msgstr "Content-Type: text/plain\\n"

msgid "%i GAME SUCCESSFULLY SCRAPED!"
msgid_plural "%i GAMES SUCCESSFULLY SCRAPED!"
msgstr[0] "%i JEU SCRAPÉ !"
msgstr[1] "%i JEUX SCRAPÉS !"`)
  })
})
