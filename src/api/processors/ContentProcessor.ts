import {jsonProcessor} from "./JsonProcessor";
import {potProcessor} from "./PotProcessor";
import {Base64} from "js-base64";


const supportedFileTypes = [
  ".json",
  ".pot",
  ".po"
]

export const isSupported = (filePath: string) => {
  for (const ext of supportedFileTypes) {
    if (filePath.endsWith(ext)) return true
  }
  return false
}

export type TranslationError = string

export type Translation = {
  id: string,
  idPlural?: string,
  str: string,
  strPlural?: string,
  comments?: string,
}

export type Translations = {
  meta: {},
  translations: { [key: string]: Translation }
}

export interface ContentProcessor {
  contentToTranslations: (blob: string) => Translations,
  translationsToContent: (translations: Translations) => string,
}


export const contentToTranslations = (content: string, filePath: string): Translations => {
  if (filePath.endsWith(".json")) {
    return jsonProcessor.contentToTranslations(content)
  }

  if (filePath.endsWith(".po") || filePath.endsWith(".pot")) {
    return potProcessor.contentToTranslations(content)
  }
  throw new Error("Unable to find file parser")
}

export const base64BlobToTranslations = (content: string, filePath: string): Translations => {
  return contentToTranslations(Base64.decode(content), filePath)
}

export const translationsToContent = (translations: Translations, filePath: string): string => {
  if (filePath.endsWith(".json")) {
    return jsonProcessor.translationsToContent(translations)
  }

  if (filePath.endsWith(".po") || filePath.endsWith(".pot")) {
    return potProcessor.translationsToContent(translations)
  }
  throw new Error("Unable to find file parser")
}

