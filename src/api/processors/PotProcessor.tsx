import {GetTextTranslation, GetTextTranslations, po} from "gettext-parser";
import {ContentProcessor, Translations} from "./ContentProcessor";

export const potProcessor: ContentProcessor = {
  contentToTranslations: (content: string): Translations => {
    const translations: Translations = {meta: {}, translations: {}}

    const parsed = po.parse(content)
    translations.meta = {...parsed.headers}

    for (const entry of Object.entries(parsed.translations[''])) {
      const tInfo = entry[1]
      if (tInfo.msgid !== "") {
        translations.translations[tInfo.msgid] = {
          id: tInfo.msgid,
          idPlural: tInfo.msgid_plural,
          str: tInfo.msgstr[0],
          strPlural: tInfo.msgstr.length > 1 ? tInfo.msgstr[1] : undefined
        }
      }
    }
    return translations
  },
  translationsToContent: (translations: Translations) => {
    let gettextTranslation: GetTextTranslations = {
      'charset': 'UTF-8',
      headers: translations.meta,
      translations: {'': {}}
    }
    for (const trans of Object.entries(translations.translations)) {
      const msgstr = trans[1].strPlural !== undefined ? [trans[1].str, trans[1].strPlural] : [trans[1].str]
      const msgid = trans[0]
      const translation: GetTextTranslation = {
        msgid,
        msgstr,
      }
      if (trans[1].idPlural) {
        translation.msgid_plural = trans[1].idPlural
      }
      gettextTranslation.translations[''][msgid] = translation
    }

    const returnTrans = po.compile(gettextTranslation).toString("UTF-8")

    return returnTrans
  }
}
