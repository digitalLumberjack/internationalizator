const FUNCTION_URL = process.env.REACT_APP_DEEPL_API_KEY ? "https://api-free.deepl.com/v2/" : "/.netlify/functions/"
export const deepl = {
  checkLanguageCompatible: (language: string): Promise<boolean> => {
    let formData = new FormData();
    formData.append("type", "target")
    if (process.env.REACT_APP_DEEPL_API_KEY) {
      formData.append("auth_key", process.env.REACT_APP_DEEPL_API_KEY)
    }
    return fetch(`${FUNCTION_URL}languages`, {
      method: "POST",
      body: formData
    }).then(res => res.json()).then(data => {
      const language2Chars = language.substring(0, 2).toUpperCase()
      const languageUpper = language.toUpperCase()
      for (const deeplSupported of data) {
        if (deeplSupported.language === language2Chars || deeplSupported.language === languageUpper)
          return true
      }
      return false
    })
  },
  translate: (source: string, desiredLang: string, sourceLang?: string): Promise<string> => {
    const lang = desiredLang.substring(0, 2).toUpperCase()
    let formData = new FormData();
    formData.append("text", source)
    formData.append("target_lang", lang)

    if (process.env.REACT_APP_DEEPL_API_KEY) {
      formData.append("auth_key", process.env.REACT_APP_DEEPL_API_KEY)
    }

    return fetch(`${FUNCTION_URL}translate`, {
      method: "POST",
      body: formData
    }).then(res => res.json()).then(data => {
      if (data.translations[0]) {
        return data.translations[0].text
      }
      return Promise.reject("Unable to fetch translation")
    })
  }
}
