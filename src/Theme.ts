import {createMuiTheme, responsiveFontSizes} from "@material-ui/core/styles";
import {green} from "@material-ui/core/colors";

export const theme = responsiveFontSizes(createMuiTheme({
  palette: {
    primary: green,
    secondary: {
      main: "#FFD200"
    },
    background: {
      default: "#f7f9fc"
    },
  },
  overrides: {
    MuiButton: {
      containedPrimary: {
          color: "white"
      }
    }
  },
  shape: {
    borderRadius: 8
  },
  typography: {
    // In Chinese and Japanese the characters are usually larger,
    // so a smaller fontsize may be appropriate.
    fontSize: 14,
    htmlFontSize: 14,
    fontFamily: "Inter",
    h1: {
      fontSize: "3rem",
      fontWeight: 700
    },
    h2: {
      fontSize: "2.5rem",
      fontWeight: 700
    },
    h3: {
      fontSize: "2.2rem",
      fontWeight: 700
    },
    h4: {
      fontSize: "1.8rem",
      fontWeight: 700
    },
    h5: {
      fontSize: "1.5rem",
      fontWeight: 400
    },
    h6: {
      fontSize: "1.25rem",
      fontWeight: 500
    }
  }
}));
