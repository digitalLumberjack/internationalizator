import React, {createContext, ReactNode, Suspense, useContext, useMemo} from 'react';
import {LoginPage} from "./features/auth/LoginPage";
import {BrowserRouter, Redirect, Route, Switch} from "react-router-dom";
import {CallbackPage} from "./features/auth/OAuthCallbackPage";
import {Layout} from "./components/Layout";
import {TranslationPage} from './features/translation/TranslationPage';
import {CssBaseline, StylesProvider, ThemeProvider} from "@material-ui/core";
import {Notifier} from "./features/notifications/Notifier";
import {ErrorBoundary} from "./components/ErrorBoundary";
import {getAuthProvider, UserInfo} from "./api/Auth";
import {WelcomePage} from "./features/welcome/WelcomePage";
import {TermFileSelectPage, TranslationFileSelectPage} from './features/urlwizard/FileSelectPage';
import {BranchSelectPage} from "./features/urlwizard/BranchSelectPage";
import {GroupSelectPage} from "./features/urlwizard/GroupSelectPage";
import {ProjectSelectPage} from "./features/urlwizard/ProjectSelectPage";
import {theme} from "./Theme";
import {TermsLanguageSelectPage, TranslationsLanguageSelectPage} from "./features/urlwizard/LanguageSelect";
import {Loading} from "./features/loading/Loading";

export const UserInfoContext = createContext<UserInfo | undefined>(undefined)


const App = () => {
  const userInfo = useMemo(() => getAuthProvider("gitlab")?.getUserInfo() || getAuthProvider("github")?.getUserInfo(), [])
  return (
    <ErrorBoundary>
      <ThemeProvider theme={theme}>
        <StylesProvider injectFirst>
          <CssBaseline/>
          <Suspense fallback={<Loading/>}>
            <UserInfoContext.Provider value={userInfo}>
              <React.Fragment>
                <Notifier>
                  <Switch>
                    <Route exact path="/login">
                      <Layout>
                        <LoginPage.Page/>
                      </Layout>
                    </Route>
                    <Route path="/oauth/callback/:provider">
                      <Layout>
                        <CallbackPage.Page/>
                      </Layout>
                    </Route>
                    <Route path={TranslationPage.getUrl()}>
                      <Layout>
                        <TranslationPage.Page/>
                      </Layout>
                    </Route>
                    <PrivateRoute path={TranslationsLanguageSelectPage.getUrl()}>
                      <Layout>
                        <TranslationsLanguageSelectPage.Page/>
                      </Layout>
                    </PrivateRoute>
                    <PrivateRoute path={TranslationFileSelectPage.getUrl()}>
                      <Layout>
                        <TranslationFileSelectPage.Page/>
                      </Layout>
                    </PrivateRoute>
                    <PrivateRoute path={TermsLanguageSelectPage.getUrl()}>
                      <Layout>
                        <TermsLanguageSelectPage.Page/>
                      </Layout>
                    </PrivateRoute>
                    <PrivateRoute path={TermFileSelectPage.getUrl()}>
                      <Layout>
                        <TermFileSelectPage.Page/>
                      </Layout>
                    </PrivateRoute>
                    <PrivateRoute path={BranchSelectPage.getUrl()}>
                      <Layout>
                        <BranchSelectPage.Page/>
                      </Layout>
                    </PrivateRoute>
                    <PrivateRoute path={ProjectSelectPage.getUrl()}>
                      <Layout>
                        <ProjectSelectPage.Page/>
                      </Layout>
                    </PrivateRoute>
                    <PrivateRoute path={GroupSelectPage.getUrl()}>
                      <Layout>
                        <GroupSelectPage.Page/>
                      </Layout>
                    </PrivateRoute>
                    <Route path={WelcomePage.getUrl()}>
                      <Layout noContainer>
                        <WelcomePage.Page/>
                      </Layout>
                    </Route>
                  </Switch>
                </Notifier>
              </React.Fragment>
            </UserInfoContext.Provider>
          </Suspense>
        </StylesProvider>
      </ThemeProvider>
    </ErrorBoundary>
  )
}

const PrivateRoute = (props: { children: ReactNode, path: string }) => {
  const userInfo = useContext(UserInfoContext)
  return (
    <Route
      path={props.path}
      render={({location}) =>
        userInfo?.token ? (
          props.children
        ) : (
          <Redirect
            to={{
              pathname: LoginPage.getUrl(),
              state: {from: location}
            }}
          />
        )
      }
    />
  )
}

export const Router = () => {
  return (
    <BrowserRouter>
      <App/>
    </BrowserRouter>
  );
}

