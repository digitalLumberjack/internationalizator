import MuiAlert from "@material-ui/lab/Alert";
import {Snackbar} from "@material-ui/core";
import React, {createContext, useMemo, useState} from "react";

export type NotificationDispatcher = { success: (message: string) => void, info: (message: string) => void, warning: (message: string, error?: Error) => void, error: (message: string, error?: Error) => void }
export type Notification = { type: "success" | "warning" | "info" | "error", message: string }

export const NotificationContext = createContext<NotificationDispatcher>({} as NotificationDispatcher)

export const Notifier = (props: { children: React.ReactNode }) => {
  const [notif, setNotif] = useState<Notification | null>(null)

  const notifier: NotificationDispatcher = useMemo(() => ({
    success: (message: string) => {
      setNotif({type: "success", message})
    }, warning: (message: string, error?: Error) => {
      setNotif({type: "warning", message})
      error && console.warn(error)
    }, error: (message: string, error?: Error) => {
      setNotif({type: "error", message})
      error && console.warn(error)
    }, info: (message: string) => {
      setNotif({type: "info", message})
    }
  }), [])

  return <NotificationContext.Provider value={notifier}>
    <Snackbar open={Boolean(notif)} autoHideDuration={6000} onClose={() => setNotif(null)}>
      <MuiAlert elevation={6} variant="filled" onClose={() => setNotif(null)} severity={notif?.type}>
        {notif?.message}
      </MuiAlert>
    </Snackbar>
    {props.children}
  </NotificationContext.Provider>

}
