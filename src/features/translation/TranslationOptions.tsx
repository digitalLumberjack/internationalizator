import {FormControlLabel, Grid, GridSize, styled, Switch} from "@material-ui/core";
import React from "react";
import {TranslationsOptions} from "./TranslationPage";
import {useTranslation} from "react-i18next";

const TranslationsActionsGrid = styled(Grid)({
  marginTop: "1em"
})

export const TranslationOptions = (props: { itemSize?: GridSize, options: TranslationsOptions, handleOptionChange: (options: TranslationsOptions) => void }) => {
  const {itemSize, options, handleOptionChange} = props
  const {t} = useTranslation()
  const sizeProps = () => {
    if (itemSize) {
      return {
        xs: itemSize
      }
    }
    return {}
  }
  return <TranslationsActionsGrid container spacing={2}>

    <Grid item {...sizeProps()}>
      <FormControlLabel
        control={<Switch
          checked={options.onlyNew}
          onChange={(ev) => handleOptionChange({...options, onlyNew: ev.currentTarget.checked})}
        />}
        label={t("Only new")}
      />
    </Grid>
    <Grid item {...sizeProps()}>
      <FormControlLabel
        control={<Switch
          checked={options.markdown}
          onChange={(ev) => handleOptionChange({...options, markdown: ev.target.checked})}
        />}
        label={t("Markdown Preview")}
      />
    </Grid>
  </TranslationsActionsGrid>
}
