import {useParams} from "react-router-dom";
import {Box, Card, CardContent, CardHeader, Fab, Grid, styled, Typography} from "@material-ui/core";
import React, {useCallback, useContext, useEffect, useState} from "react";
import {gitlabApi} from "../../api/gitlabApi";
import {UserInfoContext} from "../../App";
import {TranslationForm} from "./TranslationForm";
import {TranslationInfo} from "./TranslationInfo";
import {MarkdownPreviewCard} from "./MarkdownPreviewCard";
import {useTranslation} from "react-i18next";
import {base64BlobToTranslations, Translations, translationsToContent} from "../../api/processors/ContentProcessor";
import {SavePopup} from "./SavePopup";
import {TranslationOptions} from "./TranslationOptions";
import {BiSave, HiOutlineInformationCircle, IoOptionsOutline} from "react-icons/all";
import {SearchCard} from "../../components/SearchCard";
import {deepl} from "../../api/Deepl";
import {Pagination} from "@material-ui/lab";
import {NotificationContext} from "../notifications/Notifier";


const HeaderCard = styled(Card)({
  marginBottom: "1em"
})

const FabulousSave = styled(Fab)({
  position: "fixed",
  bottom: "2em",
  right: "2em",
  zIndex: 9999
})
const CardHeaderAlignSpan = styled("span")((theme) => ({
  '& svg': {
    verticalAlign: "middle",
    color: theme.theme.palette.secondary.main
  }
}))

const CenterPagination = styled(Pagination)({
  '& ul': {
    justifyContent: "center"
  }
})

const I17nCardHeader = (props: { icon: JSX.Element, title: string }) => {
  return <CardHeader title={<CardHeaderAlignSpan>{props.icon} {props.title}</CardHeaderAlignSpan>}/>
}

export type TranslationsOptions = { onlyNew: boolean, markdown: boolean, search: string, currentPage: number, byPage: number }
export type TranslationAutoStatus = "enabled" | "unsupportedlang" | "noapi"

const getTermToDisplay = (terms: Translations, translations: Translations, options: TranslationsOptions) => {
  const allterms = (terms: Translations, translations: Translations, onlyNew: boolean, search: string) => {
    if (search !== "") {
      return Object.entries(terms.translations).filter(t => {
        const term = t[1]
        const trans = translations.translations[t[0]]
        const searchLowerCase = search.toLowerCase()
        const found: boolean = term.id.toLowerCase().includes(searchLowerCase)
          || (term.idPlural?.toLowerCase().includes(searchLowerCase) ?? false)
          || (trans?.str?.toLowerCase().includes(searchLowerCase) ?? false)
          || (trans?.strPlural?.toLowerCase().includes(searchLowerCase) ?? false)
        return found
      }).map((t) => t[0])
    }
    if (onlyNew) {
      return Object.entries(terms.translations).filter(t => {
        const correspondingTrans = translations.translations[t[0]]
        return !correspondingTrans || correspondingTrans.str === "" || (correspondingTrans.idPlural && correspondingTrans.strPlural === "")
      }).map((t) => t[0])
    } else {
      return Object.entries(terms.translations).map((t) => t[0])
    }
  }
  const paginate = (terms: string[], currentPage: number, byPage: number) => {
    return terms.slice((currentPage - 1) * byPage, (currentPage) * byPage)
  }
  const termsToDisplay = allterms(terms, translations, options.onlyNew, options.search)
  const paginated = paginate(termsToDisplay, options.currentPage, options.byPage)
  return {
    pageTerms: paginated,
    filtered: termsToDisplay.length,
    totalPages: Math.ceil(termsToDisplay.length / options.byPage)
  }

}

const TranslationComponent = (props: { id: string, gitRef: string, termsPath: string, termsLang: string, translationsPath: string, translationsLang: string }) => {
  const {id, gitRef, translationsPath, termsPath, translationsLang, termsLang} = props
  const userInfo = useContext(UserInfoContext)
  const [translations, setTranslations] = useState<Translations | null>(null)
  const [terms, setTerms] = useState<Translations | null>(null)
  const [termsToDisplay, setTermsToDisplay] = useState<string[]>([])
  const [filtered, setFiltered] = useState(0)
  const [autoStatus, setAutoStatus] = useState<TranslationAutoStatus>("enabled")
  const [totalPages, setTotalPages] = useState(0)
  const [popupOpened, setSaveOpen] = useState(false)
  const [currentEdit, setCurrentEdit] = useState<string>("")
  const [canDisplay, setCanDisplay] = useState(false)
  const [canSave, setCanSave] = useState(false)
  const [options, setOptions] = useState<TranslationsOptions>({
    onlyNew: false,
    markdown: false,
    search: "",
    currentPage: 1,
    byPage: 20
  })
  const notif = useContext(NotificationContext)
  const {t} = useTranslation()

  const [utils] = useState({userData: userInfo, notif, t})

  const updateTermsToDisplay = useCallback((translations, forceOptions?: TranslationsOptions) => {
    if (terms && translations) {
      const res = getTermToDisplay(terms, translations, forceOptions ?? options)
      setTermsToDisplay(res.pageTerms)
      setTotalPages(res.totalPages)
      setFiltered(res.filtered)
    }
  }, [options, terms])

  useEffect(() => {
    if (canDisplay) {
      updateTermsToDisplay(translations)
      setCanDisplay(false)
    }
  }, [canDisplay, updateTermsToDisplay, translations])

  const handleOptionChange = useCallback((newOptions: TranslationsOptions) => {
    setOptions((old) => {
      if (old.search !== newOptions.search || old.onlyNew !== newOptions.onlyNew) {
        newOptions.currentPage = 1
      }
      return newOptions
    })
    updateTermsToDisplay(translations, newOptions)
  }, [updateTermsToDisplay, translations])


  useEffect(() => {
    deepl.checkLanguageCompatible(translationsLang).then((available) => {
      setAutoStatus(available ? "enabled" : "unsupportedlang")
    }, () => {
      setAutoStatus("noapi")
    })
  }, [translationsLang])

  useEffect(() => {
    const extractTranslations = (id: string, filePath: string, gitref: string, setFunction: (t: Translations) => void, token?: string) => {
      gitlabApi(token).createFetch(`/projects/${id}/repository/files/${filePath}?ref=${gitref}`)
        .then(fileData => {
          gitlabApi(token).createFetch(`/projects/${id}/repository/blobs/${fileData.blob_id}`).then(blobData => {
            try {
              const translations = base64BlobToTranslations(blobData.content, filePath)
              setFunction(translations)
            } catch (e) {
              utils.notif.warning(utils.t("Unable to process file content") + ":" + filePath)
            }
          }, error => {
            utils.notif.warning(utils.t("Unable to fetch file content") + ":" + filePath, error)
          })
        }, error => {
          utils.notif.warning(utils.t("Unable to fetch file infos") + ":" + filePath, error)
        })
    }
    const token = utils.userData?.token
    extractTranslations(id, termsPath, gitRef, (newTerms) => {
      extractTranslations(id, translationsPath, gitRef, (newTrans) => {
        setTerms(newTerms)
        setTranslations(newTrans)
        setCanDisplay(true)
      }, token)
    }, token)
  }, [id, termsPath, utils, translationsPath, gitRef, setTranslations])


  const handleTranslationChange = useCallback((term: string, value: string, plural?: boolean) => {
    if (translations && terms) {
      const termRef = terms.translations[term]
      const trans = {...translations.translations[term]}
      if (plural) {
        trans.strPlural = value
        trans.idPlural = termRef.idPlural
      } else {
        trans.str = value
      }
      const newTrans = {...translations.translations, [term]: trans}
      setTranslations({...translations, translations: newTrans})
      setCurrentEdit(value)
      setCanSave(true)
    }
  }, [translations, terms])

  const handleSaveClicked = useCallback(() => {
    setSaveOpen(true)
  }, [])

  const save = useCallback(() => {
    if (userInfo?.token && translations) {
      const toSaveTrans = translationsToContent(translations, translationsPath)
      gitlabApi(userInfo.token).createUpdate(
        `/projects/${id}/repository/files/${translationsPath}`,
        toSaveTrans,
        gitRef, userInfo.userEmail, userInfo.userName,
        "feat(i18n): update translations from internationalizator")
        .then((data) => {
          notif.success(t("File commited!"))
          setCanSave(false)
        }, (error) => {
          notif.warning(t("Unable to commit file!", error))
        })
    }
  }, [userInfo, id, notif, gitRef, translationsPath, translations, t])


  function processPercentage(terms: Translations, translations: Translations): { done: number, total: number } {
    // Terms: Pour chaque terme, je compte double si pluralid
    const totalTerms = Object.entries(terms.translations).reduce((previous, currentValue, currentIndex, array) => {
      return previous + 1 + (currentValue[1].idPlural ? 1 : 0)
    }, 0)
    // Pour chaque translation je compte 0, 1 ou 2 si plural id
    const totalTrans = Object.entries(translations.translations).reduce((previous, currentValue, currentIndex, array) => {
      return previous + ((currentValue[1].str && currentValue[1].str.length > 0) ? 1 : 0) + ((currentValue[1].strPlural && currentValue[1].strPlural.length > 0) ? 1 : 0)
    }, 0)
    return {done: totalTrans, total: totalTerms};
  }

  return (
    <React.Fragment>
      <SavePopup projectId={props.id} branchName={gitRef} filePath={translationsPath} onSave={save}
                 onClose={() => setSaveOpen(false)} open={popupOpened}/>
      <Box marginBottom={4} marginTop={4}>
        <Typography variant={"overline"}>Let's translate your document</Typography>
        <Typography variant={"h1"} gutterBottom>Translations</Typography>
      </Box>
      <FabulousSave disabled={!canSave} onClick={() => handleSaveClicked()} color={"primary"}><BiSave
        size={20}/></FabulousSave>

      <Grid container spacing={3}>
        <Grid item xs={12} sm={8}>
          <HeaderCard>
            <I17nCardHeader title="Project information" icon={<HiOutlineInformationCircle/>}/>
            <CardContent>
              <TranslationInfo totalTerms={Object.keys(terms?.translations ?? {}).length}
                               translatedPercentage={terms && translations && processPercentage(terms, translations)}
                               filtered={filtered} automaticTranslations={autoStatus}
                               termsLang={termsLang} translationsLang={translationsLang}
                               userData={userInfo} projectId={id} termPath={termsPath} filePath={translationsPath}
                               gitRef={gitRef}/>
            </CardContent>
          </HeaderCard>
        </Grid>
        <Grid item xs={12} sm={4}>
          <HeaderCard>
            <I17nCardHeader title="Options" icon={<IoOptionsOutline/>}/>
            <CardContent>
              <TranslationOptions itemSize={12} options={options} handleOptionChange={handleOptionChange}/>
            </CardContent>
          </HeaderCard>
        </Grid>
        <Grid item xs={12}>
          <SearchCard label={"Search"} onChange={(search) => handleOptionChange({...options, search})}/>
        </Grid>
        <Grid item xs={12}>
          {terms &&
          <CenterPagination defaultPage={options.currentPage}
                            page={options.currentPage}
                            count={totalPages}
                            onChange={(e, page: number) => {
                              handleOptionChange({...options, currentPage: page})
                            }}/>
          }
        </Grid>
        <Grid item xs={options.markdown ? 7 : 12}>
          {terms && translations &&
          <TranslationForm
            translationLang={translationsLang}
            filter={termsToDisplay}
            terms={terms}
            translations={translations}
            automatic={autoStatus === "enabled"}
            onChangeAndSelect={handleTranslationChange}
          />
          }
        </Grid>
        <Grid item xs={5} hidden={!options.markdown}>
          <MarkdownPreviewCard text={currentEdit}/>
        </Grid>
      </Grid>
    </React.Fragment>
  )
}

type ParamsType = { id: string, gitRef: string, termsPath: string, termsLang: string, translationsPath: string, translationsLang: string }

const ParamProxy = () => {
  const {
    id,
    gitRef,
    termsPath,
    termsLang,
    translationsPath,
    translationsLang
  } = useParams<ParamsType>()
  return <TranslationComponent id={id} gitRef={gitRef} termsPath={termsPath} termsLang={termsLang}
                               translationsPath={translationsPath} translationsLang={translationsLang}/>
}

export const TranslationPage = {
  Page: ParamProxy,
  getUrl: (params?: ParamsType) => {
    return params ?
      `/app/projects/${params.id}/branches/${params.gitRef}/terms/${params.termsPath}/lang/${params.termsLang}/translations/${params.translationsPath}/lang/${params.translationsLang}/translate`
      : "/app/projects/:id/branches/:gitRef/terms/:termsPath/lang/:termsLang/translations/:translationsPath/lang/:translationsLang/translate"
  }
}
