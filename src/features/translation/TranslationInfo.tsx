import {Box, Grid, Link, List, ListItem, ListItemIcon, ListItemText, Tooltip, Typography} from "@material-ui/core";
import React, {useContext, useEffect, useState} from "react";
import {gitlabApi} from "../../api/gitlabApi";
import {useTranslation} from "react-i18next";
import {UserInfo} from "../../api/Auth";
import {GitlabProjectInfo} from "../../types/GitlabProjectInfo";
import {
  BiStats,
  FaCodeBranch,
  FaGitlab,
  FaGlobe,
  IoFilter,
  IoLanguageOutline,
  TiThumbsDown,
  TiThumbsUp,
  TiWarningOutline
} from "react-icons/all";
import {Skeleton} from "@material-ui/lab";
import {LinearProgressWithLabel} from "../../components/LinearProgress";
import {NokChip, OkChip} from "../../components/Chips";
import {TranslationAutoStatus} from "./TranslationPage";
import Flag from "react-world-flags";
import {NotificationContext} from "../notifications/Notifier";

const findFlag = (language: string) => {
  let country = language.substring(0, 2)
  if (country === "en") {
    country = "gb"
  }
  return <Flag code={country} style={{width: "1.5em", verticalAlign: "middle"}}/>
}

export const TranslationInfo = (props: { userData?: UserInfo, translatedPercentage?: { done: number, total: number } | null, totalTerms: number, filtered: number, projectId: string, termPath: string, filePath: string, gitRef: string, automaticTranslations: TranslationAutoStatus, termsLang: string, translationsLang: string }) => {
  const {
    userData,
    projectId,
    translatedPercentage,
    termPath,
    filePath,
    gitRef,
    totalTerms,
    filtered,
    automaticTranslations,
    termsLang,
    translationsLang
  } = props
  const notif = useContext(NotificationContext)
  const {t} = useTranslation()
  const [projectInfo, setProjectInfo] = useState<GitlabProjectInfo>()
  const [canWrite, setCanWrite] = useState(false)

  useEffect(() => {
    gitlabApi(userData?.token).createFetch(`/projects/${projectId}`).then((data) => {
      setProjectInfo(data)
      console.log(data)
    }, (error) => {
      notif.warning(t("Unable to fetch project infos!", error))
    })
    gitlabApi(userData?.token).createFetch(`/projects/${projectId}/repository/branches/${gitRef}`).then((data) => {
      setCanWrite(data.can_push)
      console.log(data)
    }, (error) => {
      notif.warning(t("Unable to fetch branches infos!", error))
    })
  }, [projectId, userData, notif, t, gitRef])


  return (
    <div>
      <div>
        <List>
          <ListItem>
            <ListItemIcon>
              <FaGlobe size={20}/>
            </ListItemIcon>
            {projectInfo ?
              <ListItemText>
                <Typography>{t("Translation of")} {findFlag(termsLang)}<Link
                  href={`${projectInfo.web_url}/-/blob/${gitRef}/${termPath}`}>{unescape(termPath)}
                </Link> {t("to")} {findFlag(translationsLang)}<Link
                  href={`${projectInfo.web_url}/-/blob/${gitRef}/${filePath}`}>{unescape(filePath)}</Link></Typography>
              </ListItemText>
              : <Skeleton width={"100%"}/>
            }
          </ListItem>
          <ListItem>
            <ListItemIcon>
              <FaGitlab size={20}/>
            </ListItemIcon>
            {projectInfo ?
              <ListItemText>
                <Link href={projectInfo.web_url}>{projectInfo.web_url}</Link>
              </ListItemText>
              : <Skeleton width={"100%"}/>
            }
          </ListItem>
          <ListItem>
            <ListItemIcon>
              <FaCodeBranch size={20}/>
            </ListItemIcon>
            {projectInfo ?
              <ListItemText>
                <Link href={`${projectInfo.web_url}/-/tree/${gitRef}`}>{gitRef}</Link>
              </ListItemText>
              : <Skeleton width={"100%"}/>
            }
          </ListItem>
          <ListItem>
            <ListItemIcon>
              <IoFilter size={20}/>
            </ListItemIcon>
            <ListItemText>
              {t("Filtered")} {filtered}/{totalTerms}
            </ListItemText>
          </ListItem>
          <ListItem>
            <ListItemIcon>
              <BiStats size={20}/>
            </ListItemIcon>
            {translatedPercentage ?
              <ListItemText>
                <LinearProgressWithLabel
                  value={Math.ceil(translatedPercentage.done / translatedPercentage.total * 100)}
                  extraText={`(${translatedPercentage.done}/${translatedPercentage.total})`}/>
              </ListItemText>
              :
              <Skeleton width={"100%"}/>
            }
          </ListItem>
        </List>
        <Box marginTop={"1em"}>
          <Grid container spacing={2}>
            <Grid item>

              {canWrite ?
                <Tooltip title={<span>{t("You will be able to commit on the branch", {branch: gitRef})}</span>}><OkChip
                  size="small" label={t("Can commit")} icon={<TiThumbsUp/>}/></Tooltip>
                :
                <Tooltip
                  title={<span>{t("You won't be able to commit on the branch", {branch: gitRef})}</span>}><NokChip
                  size="small" label={t("Cannot commit")} icon={<TiWarningOutline/>}/></Tooltip>
              }
            </Grid>
            <Grid item>
              {automaticTranslations === "enabled" &&
              <Tooltip title={<span>{t("Automatic translations are available")}</span>}><OkChip
                size="small" label={t("Automatic translations")} icon={<IoLanguageOutline/>}/></Tooltip>
              }
              {automaticTranslations === "noapi" &&
              <Tooltip
                title={<span>{t("Automatic translations are not available, the api is not responding")}</span>}><NokChip
                size="small" label={t("No automatic translations")} icon={<TiThumbsDown/>}/></Tooltip>
              }
              {automaticTranslations === "unsupportedlang" &&
              <Tooltip
                title={
                  <span>{t("Automatic translations are not available, the current language is not supported")}</span>}><NokChip
                size="small" label={t("No automatic translations")} icon={<IoLanguageOutline/>}/></Tooltip>
              }
            </Grid>
          </Grid>
        </Box>
      </div>
    </div>
  )
}
