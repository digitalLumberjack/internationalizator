import {Card, CardContent, Grid, Link, makeStyles, styled, Typography} from "@material-ui/core";
import {BiKey, IoEyedropOutline, IoLanguageOutline, IoRefreshCircleOutline, IoSyncOutline} from "react-icons/all";
import {GrowGrid} from "../../components/GrowGrid";
import {FullWidthTextField} from "../../components/FullWidthTextField";
import React, {useCallback, useEffect, useState} from "react";
import {useDebouncedCallback} from "use-debounce";
import {Translation} from "../../api/processors/ContentProcessor";
import {useTranslation} from "react-i18next";
import {deepl} from "../../api/Deepl";
import {IconType} from "react-icons";

const MargedCard = styled(Card)({
  marginBottom: "1em"
})

const TranslationTextField = (props: { value: string, label: string, onSave: (value: string) => void, disabled?: boolean }) => {
  const {value, label, onSave, disabled} = props
  return (<FullWidthTextField
    label={label}
    multiline
    rowsMax={4}
    value={value}
    disabled={disabled}
    onChange={(ev) => onSave(ev.currentTarget.value)}
  />)
}

const EyeDrop = (props: { onClick?: () => void, disabled?: boolean, tooltip?: string }) => {
  const hrefProps = props.disabled ? {} : {href: "#"}
  return <Link title={props.tooltip ?? ""} color={props.disabled ? "inherit" : "primary"}
               onClick={(e) => {
                 e.preventDefault()
                 props.onClick && props.onClick()
               }} {...hrefProps}><IoEyedropOutline size={20}/></Link>
}

const useStyles = makeStyles({
  "@keyframes rotate": {
    "0%": {
      transform: "rotate(0deg)"
    },
    "100%": {
      transform: "rotate(360deg)"
    }
  },
  rotate: {
    animation: "$rotate 1s ease-in-out infinite"
  }
});

const TranslationRow = (props: { title: string, value: string, onSave?: (value: string) => void, icon: IconType, iconRotate?: boolean, iconOnClick?: () => void, disabled?: boolean, iconTooltip?: string }) => {
  const {title, value, onSave, iconRotate, iconOnClick, disabled, iconTooltip} = props
  const classes = useStyles();

  const iconsProps = iconOnClick ? {
    onClick: (e: React.MouseEvent<HTMLAnchorElement>) => {
      e.preventDefault();
      iconOnClick()
    }, color: "primary" as "primary", href: "#"
  } : {color: "inherit" as "inherit"}
  return <Grid item xs={12} container spacing={1} alignItems={"center"} wrap={"nowrap"}>
    <Grid item>
      <Link {...iconsProps}>
        <props.icon size={20} title={iconTooltip ?? ""} className={iconRotate ? classes.rotate : ""}/>
      </Link>
    </Grid>
    <GrowGrid item>
      <TranslationTextField value={value} label={title} disabled={disabled}
                            onSave={(value) => {
                              onSave && onSave(value)
                            }}/>
    </GrowGrid>
  </Grid>
}



export const TranslationCard = (props: { term: Translation, translation?: Translation, automatic: boolean, translationLang: string, onChangeAndSelect: (id: string, str: string, plural?: boolean) => void }) => {
  const {term, translation, translationLang, automatic, onChangeAndSelect} = props
  const {t} = useTranslation()
  const [singular, setSingular] = useState("")
  const [plural, setPlural] = useState("")
  const [autoTranslationText, setAutoTranslationText] = useState("")
  const [loading, setLoading] = useState(false)


  useEffect(() => {
    translation?.str && setSingular(translation.str)
    translation?.strPlural && setPlural(translation.strPlural)
  }, [translation])

  const debouncedSave = useDebouncedCallback((termId: string, text: string, plural?: boolean) => {
      onChangeAndSelect(termId, text, plural)
    },
    500
  );

  const handleAutoRequest = useCallback(() => {
    const toTranslate = term.str.length > 0 ? term.str : term.id
    setLoading(true)
    deepl.translate(toTranslate, translationLang).then((deeplTrans) => {
      setAutoTranslationText(deeplTrans)
      setLoading(false)
    })
  }, [term, translationLang])

  return <MargedCard key={term.id}>
    <CardContent>
      <Grid container spacing={1} alignItems={"center"}>
        <Grid item xs={12} container spacing={1} alignItems={"center"} wrap={"nowrap"}>
          <Grid item>
            <EyeDrop onClick={() => {
              setSingular(term.id)
              debouncedSave(term.id, term.id)
            }}
                     tooltip={t("Copy in translation text")}/>
          </Grid>
          <GrowGrid item>
            <Typography variant={"h6"} gutterBottom><BiKey/>{term.id}</Typography>
          </GrowGrid>
        </Grid>
        {term.idPlural ?
          <React.Fragment>
            {term.str.length > 0 &&
            <TranslationRow title={t("Original singular")} value={term.str}
                            icon={IoEyedropOutline} iconTooltip={t("Copy in translation text")} disabled
                            iconOnClick={() => {
                              setSingular(term.str)
                              debouncedSave(term.id, term.str)
                            }}/>
            }
            {term.strPlural && term.strPlural.length > 0 &&
            <TranslationRow title={t("Original plural")} value={term.strPlural}
                            icon={IoEyedropOutline} iconTooltip={t("Copy in translation text")} disabled
                            iconOnClick={() => {
                              setPlural(term.strPlural!)
                              debouncedSave(term.id, term.strPlural || "")
                            }}/>
            }
            <TranslationRow title={t("Singular Translation")} iconTooltip={t("Copy in translation text")}
                            value={singular} icon={IoLanguageOutline}
                            onSave={(newTrans: string) => {
                              setSingular(newTrans)
                              debouncedSave(term.id, newTrans)
                            }}/>
            <TranslationRow title={t("Plural Translation")} iconTooltip={t("Copy in translation text")} value={plural}
                            icon={IoLanguageOutline}
                            onSave={(newTrans: string) => {
                              setPlural(newTrans)
                              debouncedSave(term.id, newTrans)
                            }}/>
          </React.Fragment>
          :
          <React.Fragment>
            {/* Only Singular*/}
            {term.str.length > 0 &&
            <TranslationRow title={t("Original")} value={term.str} disabled icon={IoEyedropOutline} iconOnClick={() => {
              setSingular(term.str)
              debouncedSave(term.id, term.str)
            }}/>
            }
            {automatic &&
            <TranslationRow title={t("Deepl translation")} value={autoTranslationText}
                            icon={loading ? IoSyncOutline : autoTranslationText ? IoEyedropOutline : IoRefreshCircleOutline}
                            iconRotate={loading}
                            iconTooltip={autoTranslationText ? t("Copy in translation text") : t("Get automatic translation")}
                            iconOnClick={
                              autoTranslationText ?
                                () => {
                                  setSingular(autoTranslationText)
                                  debouncedSave(term.id, autoTranslationText)
                                }
                                :
                                handleAutoRequest
                            }
                            disabled/>
            }
            <TranslationRow title={t("Translation")} value={singular} icon={IoLanguageOutline}
                            onSave={(newTrans: string) => {
                              setSingular(newTrans)
                              debouncedSave(term.id, newTrans)
                            }}/>
          </React.Fragment>
        }
      </Grid>
    </CardContent>
  </MargedCard>
}
