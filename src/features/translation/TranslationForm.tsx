import React from "react";
import {Translations} from "../../api/processors/ContentProcessor";
import {TranslationCard} from "./TranslationCard";


export const TranslationForm = (props: {
  terms: Translations,
  translations: Translations,
  filter: string[],
  automatic: boolean,
  translationLang: string,
  onChangeAndSelect: (term: string, value: string, plural?: boolean) => void
}) => {
  const {terms, translations, filter, automatic, translationLang, onChangeAndSelect} = props

  return (
    <React.Fragment>
      {Object.entries(terms.translations)
        .filter(term => filter.includes(term[0]))
        .map((termEntry) => {
          const term = termEntry[1]
          const trans = translations.translations[term.id] || undefined
          return <TranslationCard key={term.id} term={term} translation={trans} automatic={automatic}
                                  translationLang={translationLang}
                                  onChangeAndSelect={onChangeAndSelect}/>
        })}
    </React.Fragment>
  )
}
