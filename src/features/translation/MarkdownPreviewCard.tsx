import {Card, CardContent, CardHeader, styled} from "@material-ui/core";
import ReactMarkdown from "react-markdown";
import React from "react";
import {useTranslation} from "react-i18next";

const StyledCard = styled(Card)({
  position: "sticky",
  top: "1em"
})
export const MarkdownPreviewCard = (props: { text: string }) => {
  const {t} = useTranslation()
  return <StyledCard>
    <CardContent>
      <CardHeader title={t("Markdown Preview")}/>
      <ReactMarkdown>{props.text}</ReactMarkdown>
    </CardContent>
  </StyledCard>
}
