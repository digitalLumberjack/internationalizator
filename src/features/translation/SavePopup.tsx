import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Typography
} from "@material-ui/core"
import {useContext, useEffect, useState} from "react";
import {useTranslation} from "react-i18next";
import {UserInfoContext} from "../../App";
import {GitlabProjectInfo} from "../../types/GitlabProjectInfo";
import {gitlabApi} from "../../api/gitlabApi";
import ReactMarkdown from "react-markdown";
import {OkChip} from "../../components/Chips";
import {NotificationContext} from "../notifications/Notifier";


export const SavePopup = (props: { open: boolean, projectId: string, branchName: string, filePath: string, onClose: () => void, onSave: () => void }) => {
  const {projectId, open, branchName, filePath, onClose, onSave} = props
  const {t} = useTranslation()
  const notif = useContext(NotificationContext)
  const userInfo = useContext(UserInfoContext)
  const [projectInfo, setProjectInfo] = useState<GitlabProjectInfo>()

  useEffect(() => {
    if (userInfo?.token) {
      gitlabApi(userInfo.token).createFetch(`/projects/${projectId}`).then((data) => {
        setProjectInfo(data)
      }, (error) => {
        notif.warning(t("Unable to fetch project infos!", error))
      })
    }

  }, [projectId, userInfo, notif, t])

  const handleClose = () => {
    onClose()
  };

  const markdown = `\`\`\`\n
Author: ${userInfo?.userName} <${userInfo?.userEmail}>
Date:   ${new Intl.DateTimeFormat('en-GB', {dateStyle: 'full', timeStyle: 'long'}).format(new Date())}

  feat(i18n): update translations from internationalizator

\`\`\``

  return <Dialog open={open} onClose={handleClose} maxWidth={"md"}>
    <DialogTitle>{t("Ready to contribute ?")}</DialogTitle>
    <DialogContent>
      <DialogContentText component={"div"}>
        {t("You are going to push the modifications", {file: unescape(filePath), project: projectInfo?.web_url || ""})}
        <OkChip size={"small"} label={branchName}/>.
        <Box marginTop={2}>
          <Typography>
            {t("The commit will be the following")} :
          </Typography>
        </Box>
        <ReactMarkdown>
          {markdown}
        </ReactMarkdown>
      </DialogContentText>
    </DialogContent>
    <DialogActions>
      <Button onClick={handleClose} color="primary">
        {t("Cancel")}
      </Button>
      <Button onClick={() => {
        onClose();
        onSave()
      }} color="primary" autoFocus>
        {t("Commit and Push")}
      </Button>
    </DialogActions>
  </Dialog>
}
