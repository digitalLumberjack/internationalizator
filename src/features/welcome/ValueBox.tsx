import {IconType} from "react-icons/lib";
import {Box, Color, Typography} from "@material-ui/core";
import {FeatureAvatar} from "./FeatureCard";
import React from "react";

export const ValueBox = (props: { Icon: IconType, title: string, text: string, color: Color }) => {
  const {Icon, title, text, color} = props
  return (
    <Box textAlign={"center"} marginTop={"3em"}>
      <FeatureAvatar avatarColor={color}>
        <Icon color={color["500"]} size={"1.5em"}/>
      </FeatureAvatar>
      <Box marginTop={"2em"}>
        <Typography variant={"h6"} gutterBottom> {title}</Typography>
        <Typography
          variant={"body2"}> {text}</Typography>
      </Box>
    </Box>)
}
