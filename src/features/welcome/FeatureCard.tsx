import {Avatar, CardContent, CardHeader, Color, styled, Theme, Typography} from "@material-ui/core";
import {IconType} from "react-icons/lib";
import React from "react";
import {FullHeightCard} from "../../components/FulleHeightCard";

export const FeatureCard = (props: { Icon: IconType, title: string, text: string, color: Color }) => {
  const {Icon, title, text, color} = props
  return (
    <FullHeightCard>
      <CardHeader title={<Typography variant={"h6"} gutterBottom> {title}</Typography>}
                  avatar={<FeatureAvatar avatarColor={color}>
                    <Icon color={color["500"]}/>
                  </FeatureAvatar>}/>
      <CardContent>
        <Typography
          variant={"body2"}> {text}</Typography>
      </CardContent>
    </FullHeightCard>)
}
export const FeatureAvatar = styled(Avatar)((props: { theme: Theme, avatarColor: Color }) => ({
  width: "2.5em",
  height: "2.5em",
  backgroundColor: props.avatarColor["100"],
  margin: "auto",
}))
