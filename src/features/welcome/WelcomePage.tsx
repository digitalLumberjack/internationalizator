import {
  Box,
  Button,
  Container,
  createStyles,
  Divider,
  Grid,
  makeStyles,
  styled,
  Theme,
  Typography,
  useTheme
} from "@material-ui/core";
import React, {useContext} from "react";
import {
  FaGitlab,
  IoArrowForwardOutline,
  IoColorWandOutline,
  IoCreateOutline,
  IoGitBranchOutline,
  IoGitPullRequestOutline,
  IoGlobeOutline,
  IoLanguageOutline,
  IoListOutline,
  IoLogOutOutline,
  IoRibbonOutline
} from "react-icons/all";
import {useTranslation} from "react-i18next";
import {logout} from "../../api/Auth";
import {Link} from "react-router-dom";
import {GroupSelectPage} from "../urlwizard/GroupSelectPage";
import i17rEyeSvg from "../../images/i17r-eye-full-simple-animated.svg"
import {green, lightGreen} from "@material-ui/core/colors";
import {GrowGrid} from "../../components/GrowGrid";
import {FeatureCard} from "./FeatureCard";
import {ValueBox} from "./ValueBox";
import {UserInfoContext} from "../../App";
import {TranslationPage} from "../translation/TranslationPage";


const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    button: {
      width: "100%"
    },
  }),
);

const JumboContainer = styled(Box)(({theme}) => ({
  marginTop: "3em",
  padding: "2em"
}))

const CenteredGrid = styled(Grid)({
  textAlign: "center"
})

const PaddedContainer = styled(Box)(({theme}) => ({
  paddingBottom: "3.5em",
  paddingTop: "3.5em",
  backgroundColor: theme.palette.background.paper,
}))

const SectionGrid = styled(Grid)(({theme}) => ({
  paddingTop: "3em",
}))

const Image = styled("img")({
  maxWidth: "100%"
})


const Page = () => {
  const {t} = useTranslation()
  const classes = useStyles()
  const theme = useTheme()
  const userInfo = useContext(UserInfoContext)

  const handleLogout = () => {
    logout()
  }

  return (
    <React.Fragment>
      <JumboContainer>
        <Container>
          <Grid container spacing={2} alignItems={"center"}>
            <Grid item xs={12} md={6}>
              <Typography variant={"h1"}
                          gutterBottom>{t("Hasta la vista, single language apps!")} <IoGlobeOutline
                color={theme.palette.primary.main}/></Typography>
              <Typography gutterBottom>
                {t("The Internationalizator (a.k.a. i17r) is the ultimate internationalization solution for your apps and your teams!")}
              </Typography>
              <Grid container alignItems={"center"} spacing={2}>
                <CenteredGrid item xs={12} sm={"auto"}>
                  <Button className={classes.button} variant="contained" color={"primary"}
                          component={Link} to={GroupSelectPage.getUrl()}
                          startIcon={<IoGlobeOutline/>}>{t("Start translating")}</Button>
                </CenteredGrid>
                <CenteredGrid item xs={12} sm={"auto"}>
                  <Button className={classes.button} variant="outlined" color={"primary"}
                          href={"#more"}
                          endIcon={<IoArrowForwardOutline/>}>{t("More info")}</Button>
                </CenteredGrid>
                {userInfo?.token &&
                <CenteredGrid item xs={12} sm={"auto"}>
                  <Button className={classes.button} variant="outlined" color="primary" onClick={handleLogout}
                          startIcon={<IoLogOutOutline/>}>{t("Logout")}</Button>
                </CenteredGrid>
                }
              </Grid>
            </Grid>
            <CenteredGrid item xs={12} md={6}>
              <Image src={i17rEyeSvg} alt={"Internationalizator with Arnold"}/>
            </CenteredGrid>
          </Grid>
        </Container>
      </JumboContainer>
      <PaddedContainer>
        <Container id={"more"}>
          <Grid container spacing={3}>
            <Grid item xs={12} sm={4}>
              <ValueBox Icon={IoCreateOutline}
                        title={t("Easy")}
                        text={t("Integrate non tech translators in your workflow with a simple user interface.")}
                        color={green}/>
            </Grid>
            <Grid item xs={12} sm={4}>
              <ValueBox Icon={IoGitPullRequestOutline}
                        title={t("Modern")}
                        text={t("Make the translation a part of your modern workflow.")}
                        color={green}/>
            </Grid>
            <Grid item xs={12} sm={4}>
              <ValueBox Icon={IoRibbonOutline}
                        title={t("Smart")}
                        text={t("Help translators with pre-translated propositions.")}
                        color={green}/>
            </Grid>
          </Grid>
        </Container>
      </PaddedContainer>
      <PaddedContainer>
        <Container>
          <Grid container>
            <CenteredGrid item xs={12}>
              <Typography variant={"overline"}>{t("Features")}</Typography>
            </CenteredGrid>
            <CenteredGrid item xs={12}>
              <Typography
                variant={"h2"}
                gutterBottom>{t("Coming from the future to make your app or website international!")}</Typography>
              <Box marginTop={"3em"}>
                <Button variant="contained" color={"primary"}
                        component={Link} to={TranslationPage.getUrl({
                  id: "26997430",
                  gitRef: "main",
                  termsPath: "public%2Flocales%2Fen%2Ftranslation.json",
                  termsLang: "en",
                  translationsPath: "public%2Flocales%2Ffr%2Ftranslation.json",
                  translationsLang: "fr-FR"
                })}
                        endIcon={<IoArrowForwardOutline/>}>{t("Try the demo now!")}</Button>
                <Box marginTop={"1em"}>
                  <Typography variant={"subtitle2"}>{t("Real time demo on this project translation files.")}</Typography>
                </Box>
              </Box>
            </CenteredGrid>
            <SectionGrid container item xs={12} spacing={3}>
              <Grid item xs={12} sm={6}>
                <FeatureCard Icon={IoListOutline} title={t("Modern and easy to use User Interface")}
                             text={"Non tech people will be able to create translations commits, Merge/Pull Request, and forks. Enhance productivity with options like show only new terms, translations statistics, and markdown preview"}
                             color={lightGreen}/>
              </Grid>
              <Grid item xs={12} sm={6}>
                <FeatureCard Icon={IoGitBranchOutline} title={t("Make the translations a part of your modern workflow")}
                             text={"Gitlab/Github integrations,Team members and translators can translate directly on Merge/Pull Requests, so finally, your feature branch contains translations. Deploy translations URL as Gitlab Environment for every review app"}
                             color={lightGreen}/>
              </Grid>
              <Grid item xs={12} sm={6}>
                <FeatureCard Icon={IoColorWandOutline} title={t("URL creation wizard")}
                             text={"Choose your Group, project, branch and files to create a sharable URL. Make an URL by language and share it to your team and the translators. It's enough to translate your project in a specific language."}
                             color={lightGreen}/>
              </Grid>
              <Grid item xs={12} sm={6}>
                <FeatureCard Icon={IoLanguageOutline} title={t("Automatic translation API")}
                             text={"Enhance translation productivity with automatic translations proposition for translators. Plug deepl pro api to your i17r instance."}
                             color={lightGreen}/>
              </Grid>
            </SectionGrid>
          </Grid>
        </Container>
      </PaddedContainer>
      <Divider/>
      <PaddedContainer>
        <Container>
          <Grid container>
            <GrowGrid item>
              <IoGlobeOutline
                color={theme.palette.primary.main} size={"2em"}/>
            </GrowGrid>
            <Grid item>
              <Button to={WelcomePage.getUrl()} component={Link}>HOME</Button>
              <Button href={"https://gitlab.com/digitallumberjack/internationalizator"} startIcon={<FaGitlab/>}>GITLAB
                PROJECT</Button>
            </Grid>
            <CenteredGrid item xs={12}>
              <Typography variant={"caption"}>Copyright 2020 digitalLumberjack</Typography>
            </CenteredGrid>
          </Grid>
        </Container>
      </PaddedContainer>
    </React.Fragment>
  )
}

export const WelcomePage = {
  Page: Page,
  getUrl: () => "/"
}
