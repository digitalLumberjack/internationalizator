import {Divider, List, ListItem, ListItemIcon, ListItemText} from "@material-ui/core";
import React, {useContext, useEffect, useState} from "react";
import {gitlabApi} from "../../api/gitlabApi";
import {GitlabTreeInfo} from "../../types/GitlabProjectInfo";
import {Link, useParams} from "react-router-dom";
import {UserInfoContext} from "../../App";
import {useTranslation} from "react-i18next";
import {BiFile, BiFileBlank, BiSubdirectoryRight} from "react-icons/all";
import {isSupported} from "../../api/processors/ContentProcessor";
import {lightGreen} from "@material-ui/core/colors";
import {URLWizardHeader} from "./URLWizardHeader";
import {TermsLanguageSelectPage, TranslationsLanguageSelectPage} from "./LanguageSelect";
import {NotificationContext} from "../notifications/Notifier";

const getFileLangSelectLink = (path: string, groupId: string, projectId: string, branchId: string, termsPath?: string, termsLang?: string) => {
  const encodedPath = encodeURIComponent(path)

  if (termsPath && termsLang) {
    return TranslationsLanguageSelectPage.getUrl({
      groupId,
      projectId,
      branchId,
      termsFile: termsPath,
      termsLang,
      translationsFile: encodedPath
    })
  } else {
    return TermsLanguageSelectPage.getUrl({groupId, projectId, branchId, termsFile: encodedPath})
  }
}

const createListItem = (file: GitlabTreeInfo, groupId: string, projectId: string, branchId: string, termsPath?: string, termsLang?: string) => {
  if (file.type === "tree") {
    const escapeFilePath = encodeURIComponent(file.path)
    const directoryLink = termsPath && termsLang ? TranslationFileSelectPage.getUrl({
      groupId,
      projectId,
      branchId,
      termsFile: termsPath,
      termsLang: termsLang,
      path: escapeFilePath
    }) : TermFileSelectPage.getUrl({groupId, projectId, branchId, path: escapeFilePath})
    return <ListItem button alignItems="flex-start" component={Link}
                     to={directoryLink}>
      <ListItemIcon><BiSubdirectoryRight/></ListItemIcon>
      <ListItemText>
        {file.name}
      </ListItemText>
    </ListItem>
  } else if (isSupported(file.path)) {
    return <ListItem button alignItems="flex-start"
                     component={props => <Link {...props}
                                               to={() => getFileLangSelectLink(file.path, groupId, projectId, branchId, termsPath, termsLang)}/>}>
      <ListItemIcon><BiFile color={lightGreen["600"]}/></ListItemIcon>
      <ListItemText>
        {file.name}
      </ListItemText>
    </ListItem>
  } else {
    return <ListItem alignItems="flex-start">
      <ListItemIcon><BiFileBlank/></ListItemIcon>
      <ListItemText>
        {file.name}
      </ListItemText>
    </ListItem>
  }
}
const Page = (props: { subtitle: string, groupId: string, projectId: string, branchId: string, path?: string, termsFile?: string, termsLang?: string }) => {
  const {subtitle, groupId, projectId, branchId, termsFile, termsLang, path} = props
  const userInfo = useContext(UserInfoContext)
  const notif = useContext(NotificationContext)
  const {t} = useTranslation()
  const [files, setFiles] = useState<GitlabTreeInfo[]>([])
  const [, setSearch] = useState("")
  const [utils] = useState({userInfo, notif, t})

  useEffect(() => {
    const token = utils.userInfo?.token
    if (token) {
      gitlabApi(token).createFetch(`/projects/${projectId}/repository/tree?ref=${branchId}&${path && `path=${path}`}`).then(data => {
        setFiles(data)
      }, (error) => {
        utils.notif.warning(utils.t("Unable to fetch !"), error)
      })
    }
  }, [branchId, projectId, utils, termsFile, path])

  return (
    <React.Fragment>
      <URLWizardHeader info={t(subtitle)} active={termsFile ? 4 : 3} groupId={groupId} projectId={projectId}
                       branchId={branchId} termsFile={termsFile} termsLang={termsLang}
                       setSearch={(search) => setSearch(search)}></URLWizardHeader>

      <List>
        {files.map(file => {
          return <React.Fragment>
            {createListItem(file, groupId, projectId, branchId, termsFile, termsLang)}
            <Divider component="li"/>
          </React.Fragment>
        })}
      </List>
    </React.Fragment>
  )
}


type TermFileParam = { groupId: string, projectId: string, branchId: string, path?: string }

const TermFileParamProxy = () => {
  const params = useParams<TermFileParam>()
  return <Page subtitle={"Terms file selection info"} {...params}/>
}

export const TermFileSelectPage = {
  Page: TermFileParamProxy,
  getUrl: (params?: TermFileParam) => (params ? `/app/groups/${params.groupId}/projects/${params.projectId}/branches/${params.branchId}/terms/${params.path ? `${params.path}` : ''}` : "/app/groups/:groupId/projects/:projectId/branches/:branchId/terms/:path?")
}

type TranslationFileParam = { groupId: string, projectId: string, branchId: string, termsFile: string, termsLang: string, path?: string }
const TranslationFileParamProxy = () => {
  const params = useParams<TranslationFileParam>()
  return <Page subtitle={"Translations file selection info"} {...params}/>
}
export const TranslationFileSelectPage = {
  Page: TranslationFileParamProxy,
  getUrl: (params?: TranslationFileParam) => (params ? `/app/groups/${params.groupId}/projects/${params.projectId}/branches/${params.branchId}/terms/${params.termsFile}/lang/${params.termsLang}/translations/${params.path ? `${params.path}` : ''}` : "/app/groups/:groupId/projects/:projectId/branches/:branchId/terms/:termsFile/lang/:termsLang/translations/:path?")
}
