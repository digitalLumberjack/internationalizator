import {Divider, List, ListItem, ListItemAvatar, ListItemText} from "@material-ui/core";
import React, {useContext, useEffect, useState} from "react";
import {gitlabApi} from "../../api/gitlabApi";
import {GitlabGroupInfo} from "../../types/GitlabProjectInfo";
import {Link} from "react-router-dom";
import {UserInfoContext} from "../../App";
import {useTranslation} from "react-i18next";
import {ProjectSelectPage} from "./ProjectSelectPage";
import {GroupAvatar, UserAvatar} from "../../components/Avatars";
import {URLWizardHeader} from "./URLWizardHeader";
import {NotificationContext} from "../notifications/Notifier";


const Page = () => {
  const userInfo = useContext(UserInfoContext)
  const notif = useContext(NotificationContext)
  const {t} = useTranslation()
  const [utils] = useState({userInfo, notif, t})

  const [groups, setGroups] = useState<GitlabGroupInfo[]>([])
  const [search, setSearch] = useState<string>("")
  useEffect(() => {
    const token = utils.userInfo?.token
    if (token) {
      gitlabApi(token).createFetch(`/groups?search=${search}`).then(data => {
        setGroups([{
          full_path: utils.userInfo?.userName,
          name: utils.userInfo?.userName,
          description: "Personal Projects"
        }, ...data])
      }, (error) => {
        utils.notif.warning(utils.t("Unable to fetch infos!"), error)
      })
    }
  }, [utils, search])

  return (
    <React.Fragment>
      <URLWizardHeader info={t("Group selection info")} active={0}
                       setSearch={(search) => setSearch(search)}></URLWizardHeader>
      <List>
        {groups.map((group, index) => {
          const url = index === 0 ? ProjectSelectPage.getUrl("-") : ProjectSelectPage.getUrl(group.id)
          return <React.Fragment>
            <ListItem button alignItems="flex-start" component={props => <Link {...props} to={url}/>}>
              <ListItemAvatar>
                {index === 0 ?
                  <UserAvatar token={userInfo?.token}/>
                  :
                  <GroupAvatar groupId={group.id} token={userInfo?.token}/>
                }
              </ListItemAvatar>
              <ListItemText
                primary={group.full_path}
                secondary={group.description}
              />
            </ListItem>
            <Divider variant="inset" component="li"/>
          </React.Fragment>
        })}
      </List>
    </React.Fragment>

  )
}

export const GroupSelectPage = {
  Page,
  getUrl: () => ("/app/groups")
}
