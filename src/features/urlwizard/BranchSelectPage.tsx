import {Divider, List, ListItem, ListItemText} from "@material-ui/core";
import React, {useContext, useEffect, useState} from "react";
import {gitlabApi} from "../../api/gitlabApi";
import {GitlabBranchInfo} from "../../types/GitlabProjectInfo";
import {Link, useParams} from "react-router-dom";
import { UserInfoContext} from "../../App";
import {useTranslation} from "react-i18next";
import {NokChip, OkChip} from "../../components/Chips";
import {TermFileSelectPage} from "./FileSelectPage";
import {URLWizardHeader} from "./URLWizardHeader";
import {NotificationContext} from "../notifications/Notifier";

const Page = (props: { projectId: string, groupId: string }) => {
  const {projectId,groupId} = props
  const userInfo = useContext(UserInfoContext)
  const notif = useContext(NotificationContext)
  const {t} = useTranslation()
  const [branches, setBranches] = useState<GitlabBranchInfo[]>([])

  const [utils] = useState({userInfo, notif, t})
  const [search, setSearch] = useState<string>("")

  useEffect(() => {
    const token = utils.userInfo?.token
    if (token) {
      gitlabApi(token).createFetch(`/projects/${projectId}/repository/branches?per_page=100&search=${search}`).then(data => {
        setBranches(data)
      }, (error) => {
        utils.notif.warning(utils.t("Unable to fetch project infos!"), error)
      })
    }
  }, [search, utils, projectId])
  return (
    <React.Fragment>
      <URLWizardHeader info={t("Branch selection info")} active={2} projectId={projectId} groupId={groupId}
                       setSearch={(search) => setSearch(search)}></URLWizardHeader>
      <List>
        {branches.map(branch => {
          return <React.Fragment>
            <ListItem button alignItems="flex-start" component={props => <Link {...props}
                                                                               to={TermFileSelectPage.getUrl({
                                                                                 groupId,
                                                                                 projectId,
                                                                                 branchId: branch.name
                                                                               })}/>}>
              <ListItemText>
                {branch.name} {branch.can_push ? <OkChip size={"small"} label={t("Can commit")}/> :
                <NokChip size={"small"} label={t("Cannot commit")}/>}
              </ListItemText>
            </ListItem>
            <Divider component="li"/>
          </React.Fragment>
        })}
      </List>
    </React.Fragment>
  )
}

const ParamProxy = () => {
  const params = useParams<{ projectId: string,groupId: string }>()
  return <Page {...params}/>
}

export const BranchSelectPage = {
  Page: ParamProxy,
  getUrl: (params?: {projectId: string, groupId: string}) => (params ? `/app/groups/${params.groupId}/projects/${params.projectId}/branches` : "/app/groups/:groupId/projects/:projectId/branches")
}
