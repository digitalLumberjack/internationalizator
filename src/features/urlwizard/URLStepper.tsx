import {Link as MuiLink, makeStyles, Step, StepLabel, Stepper, styled} from "@material-ui/core";
import {GroupSelectPage} from "./GroupSelectPage";
import {ProjectSelectPage} from "./ProjectSelectPage";
import {BranchSelectPage} from "./BranchSelectPage";
import {TermFileSelectPage, TranslationFileSelectPage} from "./FileSelectPage";
import {Link} from "react-router-dom";
import {Parameters} from "./URLWizardHeader";

export const steps = [
  {
    name: "Select group",
    createUrl: (params: Parameters) => GroupSelectPage.getUrl()
  },
  {
    name: "Select project",
    createUrl: (params: Parameters) => ProjectSelectPage.getUrl(params.groupId)
  },
  {
    name: "Select branch",
    createUrl: (params: Parameters) => BranchSelectPage.getUrl({groupId: params.groupId!, projectId: params.projectId!})
  },
  {
    name: "Select terms",
    createUrl: (params: Parameters) => TermFileSelectPage.getUrl({
      projectId: params.projectId!,
      branchId: params.branchId!,
      groupId: params.groupId!,
    })
  },
  {
    name: "Select translation",
    createUrl: (params: Parameters) => TranslationFileSelectPage.getUrl({
      projectId: params.projectId!,
      groupId: params.groupId!,
      branchId: params.branchId!,
      termsFile: params.termsFile!,
      termsLang: params.termsLang!,
    })
  }
]

const ResponsiveStepper = styled(Stepper)(({theme}) => ({
  [theme.breakpoints.down('xs')]: {
    justifyContent: "center",
    "& .MuiStepConnector-horizontal": {
      display: "none"
    }
  }
}))
const ResponsiveStep = styled(Step)({
  display: "block"
})


const useStyles = makeStyles((theme) => ({
  responsiveStepper: {
    [theme.breakpoints.down('xs')]: {
      display: "none"
    }
  }
}));
export const URLStepper = (props: { active: number } & Parameters) => {
  const classes = useStyles();

  return <ResponsiveStepper activeStep={props.active}>
    {steps.map((step, index) => {
      return <ResponsiveStep key={step.name} className={index === props.active ? "" : classes.responsiveStepper}>
        <StepLabel>
          {index < props.active ?
            <MuiLink component={Link} to={step.createUrl(props)}>{step.name}</MuiLink>
            :
            step.name
          }
        </StepLabel>

      </ResponsiveStep>
    })}

  </ResponsiveStepper>
}
