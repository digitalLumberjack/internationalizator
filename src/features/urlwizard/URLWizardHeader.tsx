import {Box, Card, CardContent, Grid, Typography} from "@material-ui/core";
import {URLStepper} from "./URLStepper";
import {SearchSection} from "../../components/SearchCard";
import React from "react";
import {useTranslation} from "react-i18next";
import {IoInformationCircleOutline} from "react-icons/all";
import ReactMarkdown from "react-markdown";

export type Parameters = { groupId?: string, projectId?: string, branchId?: string, termsFile?: string, termsLang?: string }

export const URLWizardHeader = (props: { active: number, setSearch: (search: string) => void, disableSearch?: boolean, info?: string } & Parameters) => {
  const {setSearch, disableSearch, info} = props
  const {t} = useTranslation()
  return <React.Fragment><Box marginBottom={4} marginTop={4}>
    <Typography variant={"overline"}>Let's create an URL</Typography>
    <Typography variant={"h1"} gutterBottom>URL Wizard</Typography>

  </Box>
    <Card>
      <CardContent>
        <URLStepper {...props}/>
        {!disableSearch &&
        <SearchSection label={t("Search")} onChange={(search) => setSearch(search)}/>
        }
      </CardContent>
    </Card>
    {info &&
    <Grid container wrap={"nowrap"} alignItems={"center"}>
      <Grid item>
        <IoInformationCircleOutline/>
      </Grid>
      <Grid item>
        <Typography variant={"subtitle1"} gutterBottom><ReactMarkdown>{info}</ReactMarkdown></Typography>
      </Grid>
    </Grid>
    }

  </React.Fragment>
}
