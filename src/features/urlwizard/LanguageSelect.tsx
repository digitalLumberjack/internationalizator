import {URLWizardHeader} from "./URLWizardHeader";
import React, {useEffect, useState} from "react";
import {Link, useParams} from "react-router-dom";
import {useTranslation} from "react-i18next";
import {Box, Button, Grid, TextField} from "@material-ui/core";
import {Autocomplete} from "@material-ui/lab";
import * as locales from '@material-ui/core/locale';
import {TranslationFileSelectPage} from "./FileSelectPage";
import {IoArrowForward} from "react-icons/all";
import {TranslationPage} from "../translation/TranslationPage";


type SupportedLocales = keyof typeof locales;

const getNextStepLink = (locale: string, groupId: string, projectId: string, branchId: string, termsPath: string, termsLang?: string, translationsPath?: string) => {
  if (translationsPath && termsLang) {
    return TranslationPage.getUrl({
      id: projectId,
      gitRef: branchId,
      termsPath: termsPath,
      termsLang,
      translationsPath,
      translationsLang: locale
    })
  } else {
    return TranslationFileSelectPage.getUrl({groupId, projectId, branchId, termsFile: termsPath, termsLang: locale})
  }
}

const localeToIso = (locale: SupportedLocales) => `${locale.substring(0, 2)}-${locale.substring(2, 4)}`

const findLocaleFromPath = (path: string): SupportedLocales => {
  // Powerful algorithm
  for (const locale of Object.entries(locales)) {
    const langCode = locale[0].substring(0, 2)
    const countryCode = locale[0].substring(2, 4)
    if (path.includes(`%2F${langCode}%2F`) || path.includes(`%2F${langCode}-${countryCode}%2F`) ||
      path.includes(`${langCode}.`) || path.includes(`${langCode}-${countryCode}.`))
      return locale[0] as SupportedLocales
  }
  return "enUS"
}

const Page = (props: { subtitle: string, groupId: string, projectId: string, branchId: string, termsFile: string, termsLang?: string, translationsFile?: string }) => {
  const {subtitle, groupId, projectId, branchId, termsFile, termsLang, translationsFile} = props
  const [locale, setLocale] = useState<SupportedLocales>("enUS")

  useEffect(() => {
    const autoLocale = findLocaleFromPath(translationsFile ?? termsFile)
    if (autoLocale)
      setLocale(autoLocale)
  }, [termsFile, translationsFile])

  return <React.Fragment>
    <URLWizardHeader info={`${subtitle}`}
                     active={translationsFile ? 4 : 3}
                     {...props}
                     setSearch={(s) => {
                     }} disableSearch></URLWizardHeader>
    <Box marginTop={2}></Box>
    <Grid container spacing={3} alignContent={"center"} alignItems={"center"} justify={"center"}>
      <Grid item>

        <Autocomplete
          options={Object.keys(locales)}
          getOptionLabel={(key) => `${key.substring(0, 2)}-${key.substring(2, 4)}`}
          style={{width: 300}}
          value={locale}
          disableClearable
          onChange={(event: any, newValue: string | null) => {
            if (newValue) {
              setLocale(newValue as SupportedLocales)
            }
          }}
          renderInput={(params) => (
            <TextField {...params} label="Language" variant="outlined" fullWidth/>
          )}
        />
      </Grid>
      <Grid item>
        <Button endIcon={<IoArrowForward/>} color={"primary"} variant={"outlined"} component={Link}
                to={getNextStepLink(localeToIso(locale), groupId, projectId, branchId, termsFile, termsLang, translationsFile)}>Next</Button>
      </Grid>
    </Grid>

  </React.Fragment>
}

const TermLanguageParamProxy = () => {
  const params = useParams<TermLanguageParam>()
  const {t} = useTranslation()

  return <Page
    subtitle={t("Choose language for file", {fileType: "terms", fileName: decodeURIComponent(params.termsFile)})}
    {...params}/>
}
type TermLanguageParam = { groupId: string, projectId: string, branchId: string, termsFile: string }

export const TermsLanguageSelectPage = {
  Page: TermLanguageParamProxy,
  getUrl: (params?: TermLanguageParam) => (params ? `/app/groups/${params.groupId}/projects/${params.projectId}/branches/${params.branchId}/terms/${params.termsFile}/lang` : "/app/groups/:groupId/projects/:projectId/branches/:branchId/terms/:termsFile/lang")
}

// Translation
const TranslationLanguageParamProxy = () => {
  const params = useParams<TranslationLanguageParam>()
  const {t} = useTranslation()
  return <Page
    subtitle={t("Choose language for file", {
      fileType: "translations",
      fileName: decodeURIComponent(params.translationsFile)
    })}
    {...params}/>
}
type TranslationLanguageParam = TermLanguageParam & { termsLang: string, translationsFile: string }

export const TranslationsLanguageSelectPage = {
  Page: TranslationLanguageParamProxy,
  getUrl: (params?: TranslationLanguageParam) => (params ? `/app/groups/${params.groupId}/projects/${params.projectId}/branches/${params.branchId}/terms/${params.termsFile}/lang/${params.termsLang}/translations/${params.translationsFile}/lang/` : "/app/groups/:groupId/projects/:projectId/branches/:branchId/terms/:termsFile/lang/:termsLang/translations/:translationsFile/lang")
}
