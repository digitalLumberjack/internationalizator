import {Divider, List, ListItem, ListItemAvatar, ListItemText} from "@material-ui/core";
import React, {useContext, useEffect, useState} from "react";
import {gitlabApi} from "../../api/gitlabApi";
import {GitlabProjectInfo} from "../../types/GitlabProjectInfo";
import {Link, useParams} from "react-router-dom";
import { UserInfoContext} from "../../App";
import {useTranslation} from "react-i18next";
import {BranchSelectPage} from "./BranchSelectPage";
import {ProjectAvatar} from "../../components/Avatars";
import {URLWizardHeader} from "./URLWizardHeader";
import {NotificationContext} from "../notifications/Notifier";

const Page = (props: { groupId: string }) => {
  const {groupId} = props
  const userData = useContext(UserInfoContext)
  const notif = useContext(NotificationContext)
  const {t} = useTranslation()
  const [utils] = useState({userData, notif, t})

  const [projects, setProjects] = useState<GitlabProjectInfo[]>([])
  const [search, setSearch] = useState<string>("")

  useEffect(() => {
    const token = utils.userData?.token
    const url = groupId !== "-" ? `/groups/${groupId}/projects?search=${search}&per_page=100` : `/users/${utils.userData?.userId}/projects?search=${search}&per_page=100`
    if (token) {
      gitlabApi(token).createFetch(url).then(data => {
        setProjects(data)
      }, (error) => {
        utils.notif.warning(utils.t("Unable to fetch project infos!"), error)
      })
    }
  }, [utils, groupId, search])
  return (
    <React.Fragment>
      <URLWizardHeader info={t("Project selection info")} active={1} groupId={groupId}
                       setSearch={(search) => setSearch(search)}></URLWizardHeader>
      <List>

        {projects.map(project => {
          return <React.Fragment>
            <ListItem key={project.id} button alignItems="flex-start"
                      component={props => <Link {...props} to={BranchSelectPage.getUrl({groupId: groupId, projectId:project.id})}/>}>
              <ListItemAvatar>
                <ProjectAvatar token={userData?.token} projectId={project.id}/>
              </ListItemAvatar>
              <ListItemText
                primary={project.name}
                secondary={project.description}
              />
            </ListItem>
            <Divider variant="inset" component="li"/>
          </React.Fragment>
        })}
      </List>
    </React.Fragment>

  )
}

const ParamsProxy = () => {
  const {groupId} = useParams<{ groupId: string }>()
  return <Page groupId={groupId}/>
}

export const ProjectSelectPage = {
  Page: ParamsProxy,
  getUrl: (groupId?: string) => (groupId ? `/app/groups/${groupId}/projects/` : "/app/groups/:groupId/projects/")
}
