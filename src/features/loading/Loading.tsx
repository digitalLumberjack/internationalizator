import {Box, Grid, styled} from "@material-ui/core";
import {IoGlobeOutline} from "react-icons/all";

const LoadingBox = styled(Box)({
  margin: "auto",
  position: "absolute",
  top: "50%",
  right: "50%",
  transform: "translateY(-50%),translateX(-50%)",
  fontSize: "2em"
})

const Logo = styled(IoGlobeOutline)(({theme}) => ({
  color: theme.palette.primary.main,
}))
export const Loading = () => {
  return <LoadingBox>
    <Grid container spacing={2}>
      <Grid item>
        <Logo/>
      </Grid>
      <Grid item>
        Loading ...
      </Grid>
    </Grid>
  </LoadingBox>
}
