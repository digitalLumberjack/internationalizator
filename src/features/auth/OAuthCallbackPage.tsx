import React, {useContext, useEffect, useState} from "react";
import {getLocalStorage} from "../../api/getLocalStorage";
import {Button} from "@material-ui/core";
import {CenteredContainer} from "../../components/CenteredContainer";
import {AiOutlineTranslation} from "react-icons/all";
import {useTranslation} from "react-i18next";
import {getAuthProvider, Provider} from "../../api/Auth";
import {useParams} from "react-router-dom";
import {useQuery} from "../../hooks/useQuery";
import {NotificationContext} from "../notifications/Notifier";

export const Page = (props: { code: string | null, state: string | null, provider: Provider }) => {
  const {code, state, provider} = props
  const authProvider = getAuthProvider(provider)

  const [buttonLink, setButtonLink] = useState("/")
  const notif = useContext(NotificationContext)
  const {t} = useTranslation()
  const [success, setSuccess] = useState(false)
  const [utils] = useState({authProvider, notif, t})

  useEffect(() => {
    if (code && state && utils.authProvider) {
      utils.authProvider.verify(code, state).then(data => {
        if (data?.token) {
          setSuccess(true)
          utils.notif.success(utils.t("Login successful!"))
        }
      })
    }
  }, [utils, code, state])

  useEffect(() => {
    const desiredLocation = getLocalStorage<string>("PostLoginLocation")
    const desiredLocationString = desiredLocation.get()
    if (desiredLocationString) {
      desiredLocation.remove()
      setButtonLink(desiredLocationString)
    }
  }, [])

  return <CenteredContainer>
    {success &&
    <Button startIcon={<AiOutlineTranslation/>} variant={"outlined"}
            href={buttonLink}>{t('Start the Internationalization!')}</Button>
    }
  </CenteredContainer>

}

const ParamProxy = () => {
  const {provider} = useParams<{ provider: string }>()
  const query = useQuery()
  const code = query.get("code")
  const state = query.get("state")
  return <Page state={state} code={code} provider={provider as Provider}/>
}

export const CallbackPage = {
  Page: ParamProxy,
  getUrl: () => {
  }
}
