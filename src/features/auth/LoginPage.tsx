import {Button, Grid, Typography} from "@material-ui/core";
import React from "react";
import {BiWorld, IoLogoGithub, IoLogoGitlab} from "react-icons/all";
import {CenteredContainer} from "../../components/CenteredContainer";
import {useTranslation} from "react-i18next";
import {getAuthProvider} from "../../api/Auth";
import {useLocation} from "react-router-dom";
import {getLocalStorage} from "../../api/getLocalStorage";

export const Page = () => {
  const {t} = useTranslation();
  const location = useLocation<{ from: { pathname: string } }>();

  const gitlabAuthProvider = getAuthProvider("gitlab", true)
  const githubAuthProvider = getAuthProvider("github", true)


  if (location?.state?.from) {
    const desiredLocation = getLocalStorage<string>("PostLoginLocation")
    desiredLocation.set(location.state.from.pathname)
  }

  return (
    <CenteredContainer>
      <Typography variant={"h4"} component={"h1"}
                  gutterBottom>{t("Welcome to")} Internationalizator <BiWorld/></Typography>
      <Grid container spacing={2} alignItems={"center"} justify={"center"}>
        {gitlabAuthProvider &&
        <Grid item>
          <Button variant="contained" color="primary" component={"a"} href={gitlabAuthProvider.getOauthUrl()}
                  startIcon={<IoLogoGitlab/>}>{t("Login to Gitlab")}</Button>
        </Grid>
        }
        {githubAuthProvider &&
        <Grid item>
          <Button variant="contained" color="primary" component={"a"} href={githubAuthProvider.getOauthUrl()}
                  startIcon={<IoLogoGithub/>}>{t("Login to Github")}</Button>
        </Grid>
        }
      </Grid>
    </CenteredContainer>
  )
}

export const LoginPage = {
  Page: Page,
  getUrl: () => "/login"
}
