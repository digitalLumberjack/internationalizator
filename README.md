![](./assets/internationalizator.png)


**The Internationalizator (a.k.a. i17r) is the ultimate internationalization solution for your apps and your teams!**
https://internationalizator.netlify.app/

This app is a frontend dataless application.


### The promises

- Easy
  - Integrate non tech translators in your workflow with a simple user interface.
- Modern
  - Make the translation a part of your modern workflow.
- Intelligent
  - Help translators with pre-translated propositions. 
  

### Features

- Modern and easy to use **User Interface** for your translations:  
  - Non tech people will be able to create translations commits, Merge/Pull Request, and forks
  - Enhance productivity with options like show only new terms, translations statistics, and markdown preview
- Make the translations a part of your **modern workflow**:
  - Gitlab/Github integrations
  - Team members and translators can translate directly on Merge/Pull Requests, so finally, your feature branch contains translations.
  - Deploy translations URL as Gitlab Environment for every review app
- Create a sharable URL with the **URL creation wizard**:
  - Choose your Group, project, branch and files to create a sharable URL
  - Make an URL by language and share it to your team and the translators. It's enough to translate your project in a specific language.
- **Automatic translation** API
  - Enhance translation productivity with automatic translations proposition for translators
  

### How the URL is created ?

The URL is formed with all necessary information:
- the project id
- the git ref (branch)
- the file to get terms (keys) from (URL encoded)
- the language of the terms file   
- the file to get and save translated string from (URL encoded)
- the language of the translations file

Share this simple url to your translators, and they can begin the translation directly in the interface!
```
https://internationalizator.netlify.app/app/projects/PROJECT_ID/branches/GIT_REF/terms/TERMS_FILE/lang/TERMS_LANG/translations/TRANSLATE_FILE/lang/TRANSLATIONS_LANG/translate
```


### Exemple on this project

- the project id: `26997430`
- the git ref (branch): `main`
- the file to get terms (keys) from (URL encoded): `public%2Flocales%2Fen%2Ftranslation.json`
- the file to get and save translated string from (URL encoded): `public%2Flocales%2Ffr%2Ftranslation.json`

So if you want to translate directly to french or share the URL to your team for translations, you can use:  
https://internationalizator.netlify.app/app/projects/26997430/branches/main/terms/public%2Flocales%2Fen%2Ftranslation.json/lang/en-US/translations/public%2Flocales%2Ffr%2Ftranslation.json/lang/fr-FR/translate

On first visit, the application will ask you to log in with your gitlab account.

If the account can write to the specified branch, you can proceed to the translation and commit you changes.


### Deploy your instance on Netlify

#### 1. Create your Gitlab application

Create a user, a group, or an instance Gitlab application, setting the scope to `api`:
- Group application: https://docs.gitlab.com/ee/integration/oauth_provider.html#group-owned-applications
- User Application: https://docs.gitlab.com/ee/integration/oauth_provider.html#user-owned-applications

#### 2. Deploy on netlify
Then deploy the new instance on netlify, the deploy process will ask you for your application `Application ID` and `Secret` variables.

If you want to enable the **Deepl Automatic Translation** feature, set the `DEEPL_API_KEY` and `DEEPL_API_ENDPOINT` variables. It will enable the `Automatic Translation` button in the frontend, 
and the serverless functions that proxify Deepl API requests.

[![deploy on netlify](https://www.netlify.com/img/deploy/button.svg)](https://app.netlify.com/start/deploy?repository=https://gitlab.com/digitalLumberjack/internationalizator)

#### 3. The callback URL

The callback URL is `https://[your-instance-url]/oauth/callback`
Once your instance is deployed, you'll have to update the callback URL in your Gitlab application.


### Environment variables

- `REACT_APP_DEEPL_API_KEY`: only for development purpose. When set, it will override the Deepl proxy functions and call directly Deepl API.
- `REACT_APP_REDIRECT_URI`: force OAuth redirect URL
- `DEEPL_API_KEY`: your Deepl API key
- `DEEPL_API_ENDPOINT`: `https://api-free.deepl.com/v2/` or `https://api.deepl.com/v2/`
- `REACT_APP_APP_ID_[PROVIDER]`: set OAuth app id for `PROVIDER` (gitlab)
- `REACT_APP_APP_SECRET_[PROVIDER]`: set OAuth app secret for `PROVIDER` (gitlab)


### Development

#### Start the application
```shell
REACT_APP_APP_ID_GITLAB=XX REACT_APP_APP_SECRET_GITLAB=XX \
REACT_APP_REDIRECT_URI="http://localhost:3000/oauth/callback" REACT_APP_DEEPL_API_KEY=XX \
yarn start
```


### Interested in this project ?

What could we do next :
- [ ] Auto fork and MR when the user cannot write on the branch
- [ ] Order by last modified terms or translations
- [ ] Add a github provider
- [ ] Search files
- [ ] Add more files support (.toml)
- [ ] File history (last commits)
- [ ] Translation History (last commit and author)
- [ ] Show modified terms in push popup
- [ ] Show link between current translation and markdown preview
- [ ] Infinite scroll on translations ?
- [ ] Animation about how to translate for the welcome page
- [X] Links on URL wizard stepper
- [X] Pagination on translations
- [X] Flags in translation info
- [X] Back button on file browsing
- [X] DeepL Api integration for automatic translation proposition
- [X] Welcome page with product presentation
- [X] Add statistics about the translation
- [X] Search terms
- [X] Toggle only empty translations
- [X] Toggle markdown preview
- [X] Deploy on netlify
- [X] Filter supported file types on file selection
- [X] Add po and pot support
- [X] Popup before commit
- [X] Stepper for url wizard
- [X] Project browsing and file selection for url generation
- [X] Group projects
- [X] Search projects, groups, branches
